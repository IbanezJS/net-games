﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace SwarmBattle.Core
{
    public class BattleActionInfo
    {
        //this setted in DoBattleAction
        public readonly List<EntityPair> ShootPairs;
        public readonly List<EntityPair> HealPairs;
        public readonly List<EntityPair> InformationPairs;
        public readonly List<MoveRealInfo> RealMoves;

        //this setted in Commit
        public readonly HashSet<CellEntity> DeadEntities;
        public readonly double[] TotalSwarmHealth; //no need to clear because all reset
        public readonly double[] TotalSwarmPower;
        public readonly double[] TotalSwarmMana;
        public int WinSwarmIndex;

        public BattleActionInfo(int maxEntityCount, int swarmCount)
        {
            ShootPairs = new List<EntityPair>(32);
            HealPairs = new List<EntityPair>(32);
            InformationPairs = new List<EntityPair>(32);
            RealMoves = new List<MoveRealInfo>(maxEntityCount);
            DeadEntities = new HashSet<CellEntity>();
            TotalSwarmHealth = new double[swarmCount];
            TotalSwarmPower = new double[swarmCount];
            TotalSwarmMana = new double[swarmCount];
        }

        public void Clear()
        {
            ShootPairs.Clear();
            HealPairs.Clear();
            InformationPairs.Clear();
            RealMoves.Clear();
            DeadEntities.Clear();
            WinSwarmIndex = -1;
        }

        public void Merge(BattleActionInfo other)
        {
            ShootPairs.AddRange(other.ShootPairs);
            HealPairs.AddRange(other.HealPairs);
            InformationPairs.AddRange(other.InformationPairs);
            RealMoves.AddRange(other.RealMoves);
        }
    }

    public class BattleWorld
    {
        private class ThreadContext
        {
            public readonly int ThreadIndex;
            public readonly Random Rnd;
            public readonly BattleActionInfo LocalActionInfo;
            public readonly ManualResetEvent StartIterationWaitEvent;
            public readonly ManualResetEvent EndIterationWaitEvent;
            
            public readonly List<KeyValuePair<BattleEntity, InformationInfo>> InformationMap;

            public ThreadContext(
                int threadIndex,
                BattleActionInfo localActionInfo,
                ManualResetEvent startIterationWaitEvent,
                ManualResetEvent endIterationWaitEvent)
            {
                ThreadIndex = threadIndex;
                Rnd = new Random(Guid.NewGuid().GetHashCode());
                LocalActionInfo = localActionInfo;
                StartIterationWaitEvent = startIterationWaitEvent;
                EndIterationWaitEvent = endIterationWaitEvent;
                InformationMap = new List<KeyValuePair<BattleEntity, InformationInfo>>();
            }
        }

        public BattleSettings Settings { get; }
        private int entityIdCounter;
        private readonly List<BattleEntity> allEntitiesRndList; //keep only alive entities
        private readonly BattleEntity[][] battleField; //keep only alive entities

        private readonly double[] totalHealth; 
        private readonly double[] totalPower; 
        private readonly double[] totalMana;
        
        private readonly ThreadContext mainContext;
        
        private readonly ConcurrentDictionary<Cell, int> toMoveSet = new ConcurrentDictionary<Cell, int>(); //protect against overlap
        
        public List<List<BattleEntity>> Swarms { get; } //keep only alive entities
        private readonly List<Type> swarmTypes;

        private readonly Dictionary<int, List<Cell>> toAddManual;
        public event Action<CellEntity> OnEntityAddManual;

        private volatile bool running;
        private long exitCounter;
        private Thread thread; 
        private Thread[] threads; 
        private ThreadContext[] threadContexts;
        private ManualResetEvent[] threadsWorkEndWait;
        private readonly ConcurrentQueue<BattleActionInfo> actionResultPool = new ConcurrentQueue<BattleActionInfo>();
        private volatile int maxCalcMoveCacheCount = 2; 
        public readonly ConcurrentQueue<BattleActionInfo> MovesCache = new ConcurrentQueue<BattleActionInfo>();

        public BattleWorld(BattleSettings settings)
        {
            Settings = settings;
            int allEntitiesCount = settings.SwormStrategies.Count * settings.EntityCount;
            Width = settings.Width;
            Height = settings.Height;
            if (allEntitiesCount > Width * Height)
                throw new InvalidOperationException($"TOO MANY ENTITIES: {allEntitiesCount} > {Width*Height}");
            Swarms = new List<List<BattleEntity>>(settings.SwormStrategies.Count);
            swarmTypes = new List<Type>(settings.SwormStrategies.Count);
            toAddManual = new Dictionary<int, List<Cell>>();
            allEntitiesRndList = new List<BattleEntity>(allEntitiesCount);
            var allCells = new Cell[Width * Height];
            for (int y = 0; y < settings.Height; y++)
            {
                for (int x = 0; x < settings.Width; x++)
                {
                    allCells[y * settings.Width + x] = new Cell(x, y);
                }
            }
            var rnd = new Random(Guid.NewGuid().GetHashCode());
            Helper.DoRandomPermutation(allCells, rnd, 0, allCells.Length);
            battleField = new BattleEntity[Height][];
            for (int y = 0; y < Height; y++)
            {
                battleField[y] = new BattleEntity[Width];
            }

            var typeMap = Helper.GetStrategyTypes();
            entityIdCounter = 0;
            int swormCount = settings.SwormStrategies.Count;
            totalHealth = new double[swormCount];
            totalPower = new double[swormCount];
            totalMana = new double[swormCount];
            
            for (var swarmId = 0; swarmId < swormCount; swarmId++)
            {
                var strategyTypeStr = settings.SwormStrategies[swarmId];
                var type = typeMap[strategyTypeStr];
                var swormList = new List<BattleEntity>(settings.EntityCount);
                Swarms.Add(swormList);
                swarmTypes.Add(type);
                for (int i = 0; i < settings.EntityCount; i++)
                {
                    var strategy = (IBattleStrategy) Activator.CreateInstance(type);
                    var rndCell = allCells[entityIdCounter];
                    var entity = new BattleEntity(this, strategy, entityIdCounter, swarmId, rndCell, settings.EntitySettings.Clone());
                    allEntitiesRndList.Add(entity);
                    swormList.Add(entity);
                    battleField[rndCell.Y][rndCell.X] = entity;
                    ++entityIdCounter;
                }
            }

            foreach (var entity in allEntitiesRndList)
            {
                entity.Strategy.SetCheatingWorld(this, entity); // for cheating strategies
            }

            mainContext = new ThreadContext(-1, new BattleActionInfo(allEntitiesCount, swormCount), null, null);
        }

        public int Width { get; }
        public int Height { get; }

        public void SetPreCalcCapacity(int capacity)
        {
            if (capacity <= 0)
            {
                maxCalcMoveCacheCount = 2;
                return;
            }
            maxCalcMoveCacheCount = capacity;
        }

        public void StartSimulation()
        {
            running = true;

            if (Settings.ThreadCount <= 0)
                Settings.ThreadCount = Environment.ProcessorCount;
            threads = new Thread[Settings.ThreadCount];
            threadContexts = new ThreadContext[Settings.ThreadCount];
            threadsWorkEndWait = new ManualResetEvent[Settings.ThreadCount];
            int allEntitiesCount = Settings.SwormStrategies.Count * Settings.EntityCount;
            allEntitiesCount = (int)((double)allEntitiesCount / Settings.ThreadCount) + 1;
            exitCounter = 1 + Settings.ThreadCount;
            for (int i = 0; i < Settings.ThreadCount; i++)
            {
                var startIterationWait = new ManualResetEvent(false);
                var endIterationWait = new ManualResetEvent(false);
                threadsWorkEndWait[i] = endIterationWait;
                threadContexts[i] = new ThreadContext(i, 
                    new BattleActionInfo(allEntitiesCount, Settings.SwormStrategies.Count),
                    startIterationWait,
                    endIterationWait);
                threads[i] = new Thread(BattleActionCalcLoop) {IsBackground = true};
                threads[i].Start(threadContexts[i]);
            }

            thread = new Thread(MainCalcLoop){IsBackground = true};
            thread.Start();
        }

        public void StopSimulation()
        {
            running = false;
            while (Interlocked.Read(ref exitCounter) > 0)
            {
                Thread.Sleep(1);
            }
        }

        private void MainCalcLoop()
        {
            while (running)
            {
                if (MovesCache.Count >= maxCalcMoveCacheCount)
                {
                    Thread.Sleep(0);
                    continue;
                }

                toMoveSet.Clear();
                
                foreach (var ctx in threadContexts)
                {
                    ctx.StartIterationWaitEvent.Set();
                }

                //wait for all threads to complete
                WaitHandle.WaitAll(threadsWorkEndWait);
                foreach (var handle in threadsWorkEndWait)
                {
                    handle.Reset();
                }

                if (!actionResultPool.TryDequeue(out var result))
                    result = new BattleActionInfo(Settings.SwormStrategies.Count * Settings.EntityCount, Settings.SwormStrategies.Count);

                result.Clear();
                mainContext.InformationMap.Clear();
                foreach (var ctx in threadContexts)
                {
                    result.Merge(ctx.LocalActionInfo);
                    mainContext.InformationMap.AddRange(ctx.InformationMap);
                }

                CommitMove(result); //dead entities setted here
                
                MovesCache.Enqueue(result);
            }

            foreach (var ctx in threadContexts)
            {
                ctx.StartIterationWaitEvent.Set();
            }

            Interlocked.Decrement(ref exitCounter);
        }

        public void ReleaseBattleAction(BattleActionInfo info)
        {
            actionResultPool.Enqueue(info);
        }

        private void BattleActionCalcLoop(object data)
        {
            var ctx = (ThreadContext) data;
            while (running)
            {
                ctx.StartIterationWaitEvent.WaitOne();
                ctx.StartIterationWaitEvent.Reset();

                //settings.ThreadCount
                int allCount = allEntitiesRndList.Count;
                int perThread = (int)((double)allCount / Settings.ThreadCount) + 1;

                int startIndex = ctx.ThreadIndex * perThread;
                int count = Math.Max(0, Math.Min(allCount - startIndex, perThread));

                DoBattleActionInner(ctx, startIndex, count); 

                ctx.EndIterationWaitEvent.Set();
            }
            Interlocked.Decrement(ref exitCounter);
        }

        private bool CellInBound(int x, int y)
        {
            return x >= 0 && x < Width && y >= 0 && y < Height;
        }

        public bool CanMoveToCell(int x, int y)
        {
            return CellInBound(x, y) && battleField[y][x] == null && !toMoveSet.ContainsKey(new Cell(x,y));
        }

        public IEnumerable<CellInfo> GetVision(Cell fromCell)
        {
            var me = battleField[fromCell.Y][fromCell.X];
            Debug.Assert(me != null);

            for (int dy = -Settings.EntitySettings.Vision; dy < Settings.EntitySettings.Vision; ++dy)
            {
                for (int dx = -Settings.EntitySettings.Vision; dx < Settings.EntitySettings.Vision; ++dx)
                {
                    if (dx == 0 && dy == 0)
                        continue;
                    int x = fromCell.X + dx;
                    int y = fromCell.Y + dy;
                    if (!CellInBound(x, y))
                        continue;
                    if (fromCell.Distance(x, y) > Settings.EntitySettings.Vision)
                        continue;
                    var entity = battleField[y][x];
                    bool isEmpty = entity == null;
                    bool isEnemy = !isEmpty && entity.SwarmId != me.SwarmId;
                    bool isFriend = !isEmpty && entity.SwarmId == me.SwarmId;
                    CellEntity clone = default;
                    if (!isEmpty)
                        clone = entity.Clone();
                    yield return new CellInfo
                    {
                        Cell = new Cell(x,y),
                        IsEmpty =  isEmpty,
                        IsEnemy =  isEnemy,
                        IsFriend = isFriend,
                        Entity = clone
                    };
                }
            }
        }

        /// <summary>
        /// WARNING: not store return result! because it will be reused
        /// </summary>
        public BattleActionInfo DoBattleAction()
        {
            toMoveSet.Clear();
            
            DoBattleActionInner(mainContext, 0, allEntitiesRndList.Count);

            return mainContext.LocalActionInfo;
        }

        //this can be do in parallel
        private void DoBattleActionInner(ThreadContext ctx, int startIndex, int itemCount)
        {
            ctx.LocalActionInfo.Clear();
            ctx.InformationMap.Clear();

            if (itemCount <= 0)
                return;
            
            Helper.DoRandomPermutation(allEntitiesRndList, ctx.Rnd, startIndex, itemCount);

            for (var i = 0; i < itemCount; i++)
            {
                var entity = allEntitiesRndList[startIndex + i];
                
                var moveInfo = entity.MakeMove();
                if (Settings.AllowFire && moveInfo.IsFire && entity.CanFire && CellInBound(moveInfo.FireCell.X, moveInfo.FireCell.Y))
                {
                    var otherEntity = battleField[moveInfo.FireCell.Y][moveInfo.FireCell.X];
                    if (otherEntity != null && entity.IsEnemyFireable(otherEntity))
                    {
                        CellEntity source;
                        CellEntity target;
                        lock (entity.LockObj)
                        {
                            if (!Settings.InfinitePower)
                                entity.Power = 0;
                            source = entity.Clone();
                        }
                        lock (otherEntity.LockObj)
                        {
                            otherEntity.Health -= Settings.EntitySettings.FireDamage;
                            target = otherEntity.Clone();
                        }
                        ctx.LocalActionInfo.ShootPairs.Add(new EntityPair(source, target));
                    }
                }

                if (Settings.AllowHeal && moveInfo.IsHeal && entity.CanHeal && CellInBound(moveInfo.HealCell.X, moveInfo.HealCell.Y))
                {
                    var otherEntity = battleField[moveInfo.HealCell.Y][moveInfo.HealCell.X];
                    if (otherEntity != null && entity.IsFriendHealable(otherEntity)) 
                    {
                        CellEntity source;
                        CellEntity target;
                        lock (entity.LockObj)
                        {
                            if (!Settings.InfiniteMana)
                                entity.Mana = 0;
                            source = entity.Clone();
                        }
                        lock (otherEntity.LockObj)
                        {
                            otherEntity.Health += Settings.EntitySettings.HealFactor;
                            target = otherEntity.Clone();
                        }
                        ctx.LocalActionInfo.HealPairs.Add(new EntityPair(source, target));
                    }
                }

                if (moveInfo.IsInformation && entity.CanSendInformation && CellInBound(moveInfo.Information.TargetCell.X, moveInfo.Information.TargetCell.Y))
                {
                    var otherEntity = battleField[moveInfo.Information.TargetCell.Y][moveInfo.Information.TargetCell.X];
                    if (otherEntity != null && entity.IsFriendInform(otherEntity))
                    {
                        CellEntity source;
                        CellEntity target;
                        lock (entity.LockObj)
                        {
                            if (!Settings.InfinitePower)
                                entity.Power -= Settings.EntitySettings.InformationPowerPenalty;
                            source = entity.Clone();
                        }
                        lock (otherEntity.LockObj)
                        {
                            target = otherEntity.Clone();
                        }
                        ctx.InformationMap.Add(new KeyValuePair<BattleEntity, InformationInfo>(otherEntity, moveInfo.Information));
                        ctx.LocalActionInfo.InformationPairs.Add(new EntityPair(source, target));
                    }
                }

                lock (entity.LockObj)
                {
                    entity.Power += Settings.EntitySettings.PowerRestorePerMove;
                    entity.Power = Math.Min(1, entity.Power);
                    entity.Mana += Settings.EntitySettings.ManaRestorePerMove;
                    entity.Mana = Math.Min(1, entity.Mana);
                }
                
                int dx = 0;
                int dy = 0;
                if (moveInfo.Direction != Direction.None)
                {
                    bool isRealBoosted = moveInfo.IsBoosted && entity.CanBoost;
                    //who first to that cell in random order
                    if (entity.CanMove(moveInfo.Direction, isRealBoosted))
                    {
                        Helper.ConvertDirection(moveInfo.Direction, out dx, out dy);
                        Debug.Assert(dx != 0 || dy != 0);
                        var targetCell = new Cell(entity.Position.X + dx, entity.Position.Y + dy);
                        if (!toMoveSet.TryAdd(targetCell, 0)) //optimistic parallel
                            dx = dy = 0;
                        else if (isRealBoosted)
                        {
                            dx *= 2;
                            dy *= 2;
                            var targetCell2 = new Cell(entity.Position.X + dx, entity.Position.Y + dy);
                            if (!toMoveSet.TryAdd(targetCell2, 0)) //optimistic parallel
                                dx = dy = 0;
                            else
                            {
                                lock (entity.LockObj)
                                {
                                    if (!Settings.InfinitePower)
                                        entity.Power -= Settings.EntitySettings.BoostPowerPenalty;
                                }
                            }
                        }
                    }
                }

                CellEntity entitySnap;
                lock (entity.LockObj)
                {
                    entitySnap = entity.Clone();
                }
                ctx.LocalActionInfo.RealMoves.Add(new MoveRealInfo(entitySnap, dx, dy));
            }
        }

        private void RemoveEntityInternal(CellEntity entitySnap)
        {
            var entity = battleField[entitySnap.Position.Y][entitySnap.Position.X];
            Debug.Assert(entity != null);
            Debug.Assert(entitySnap.Position.Equals(entity.Position));
            bool removed1 = Swarms[entity.SwarmId].Remove(entity);
            bool removed2 = allEntitiesRndList.Remove(entity);
            Debug.Assert(removed1 && removed2);
            Debug.Assert(battleField[entity.Position.Y][entity.Position.X] == entity);
            battleField[entity.Position.Y][entity.Position.X] = null;
        }

        //always single thread
        public bool CommitMove(BattleActionInfo actionInfo)
        {
            foreach (var pair in mainContext.InformationMap)
            {
                Debug.Assert(pair.Key.Position.Equals(pair.Value.TargetCell));
                pair.Key.SetInformation(pair.Value.Information);
            }

            //commit moves
            foreach (var move in actionInfo.RealMoves)
            {
                var prevCell = move.Entity.Position;
                var newCell = new Cell(prevCell.X + move.Dx, prevCell.Y + move.Dy);

                Debug.Assert(battleField[prevCell.Y][prevCell.X].Id == move.Entity.Id);
                var entityToMove = battleField[prevCell.Y][prevCell.X];
                entityToMove.Position = newCell;

                battleField[prevCell.Y][prevCell.X] = null;

                Debug.Assert(battleField[newCell.Y][newCell.X] == null);
                battleField[newCell.Y][newCell.X] = entityToMove;
            }

            actionInfo.WinSwarmIndex = -1;
            var notEmptyCount = Swarms.Count(s => s.Count > 0);
            bool isEnd = false;
            if (notEmptyCount <= 1 && Swarms.Count > 1)
            {
                isEnd = true;
                actionInfo.WinSwarmIndex = Swarms.FindIndex(s => s.Count > 0);
            }

            Array.Clear(totalHealth, 0, totalHealth.Length);
            Array.Clear(totalPower, 0, totalPower.Length);
            Array.Clear(totalMana, 0, totalMana.Length);
            //enumeration to get dead mans after all actions
            foreach (var entity in allEntitiesRndList)
            {
                entity.Health = Math.Max(0, Math.Min(1, entity.Health)); //in case too match heal or dead;
                
                totalHealth[entity.SwarmId] += entity.Health;
                totalPower[entity.SwarmId] += entity.Power;
                totalMana[entity.SwarmId] += entity.Mana;

                if (entity.Health <= 0)
                    actionInfo.DeadEntities.Add(entity.Clone());

                bool? isSwarmWin = null;
                if (isEnd)
                    isSwarmWin = entity.SwarmId == actionInfo.WinSwarmIndex;
                entity.OnMoveEnd(entity.Health <= 0, isSwarmWin);
            }

            for (int i = 0; i < Swarms.Count; i++)
            {
                double maxCount = Math.Max(Swarms[i].Count, Settings.EntityCount);
                actionInfo.TotalSwarmHealth[i] = totalHealth[i] / maxCount;
                actionInfo.TotalSwarmPower[i] = totalPower[i] / maxCount;
                actionInfo.TotalSwarmMana[i] = totalMana[i] / maxCount;
            }
            
            foreach (var entitySnap in actionInfo.DeadEntities)
            {
                RemoveEntityInternal(entitySnap);
            }

            lock (toAddManual)
            {
                foreach (var kvp in toAddManual)
                {
                    foreach (var cell in kvp.Value)
                    {
                        AddEntityInner(kvp.Key, cell.X, cell.Y);
                    }
                }
                toAddManual.Clear();
            }

            return isEnd;
        }

        public void AddEntity(int swarmId, int xCell, int yCell)
        {
            lock (toAddManual)
            {
                if (!toAddManual.ContainsKey(swarmId))
                    toAddManual.Add(swarmId, new List<Cell>());
                toAddManual[swarmId].Add(new Cell(xCell, yCell));
            }
        }

        private void AddEntityInner(int swarmId, int xCell, int yCell)
        {
            if (!CellInBound(xCell, yCell))
                return;
            if (battleField[yCell][xCell] != null)
                return;
            var strategy = (IBattleStrategy)Activator.CreateInstance(swarmTypes[swarmId]);
            var entity = new BattleEntity(this, strategy, entityIdCounter, swarmId, new Cell(xCell, yCell), Settings.EntitySettings.Clone());
            allEntitiesRndList.Add(entity);
            Swarms[swarmId].Add(entity);
            battleField[yCell][xCell] = entity;
            ++entityIdCounter;
            entity.Strategy.SetCheatingWorld(this, entity); // for cheating strategies
            OnEntityAddManual?.Invoke(entity.Clone());
        }

        public BattleEntity GetEntity(int x, int y)
        {
            if (!CellInBound(x, y))
                return null;
            return battleField[y][x];
        }

        public bool IsTrap(BattleEntity entity, Direction dirX, Direction dirY)
        {
            return (dirX == Direction.None || !entity.CanMove(dirX, false)) && (dirY == Direction.None || !entity.CanMove(dirY, false));
        }
    }
}