﻿using System;

namespace SwarmBattle.Core
{
    public struct Cell
    {
        public readonly int X;
        public readonly int Y;

        public Cell(int x, int y)
        {
            X = x;
            Y = y;
        }

        public bool Equals(Cell other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            return obj is Cell other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Y;
            }
        }

        public double Distance(int x, int y)
        {
            int dx = X - x;
            int dy = Y - y;
            return Math.Sqrt(dx*dx + dy*dy);
        }

        public double Distance(Cell cell)
        {
            return Distance(cell.X, cell.Y);
        }
    }
}