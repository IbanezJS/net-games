﻿namespace SwarmBattle.Core
{
    public enum Direction
    {
        None,
        Up,
        Down,
        Left,
        Right
    }

    public struct MoveInfo
    {
        public Direction Direction;
        public bool IsBoosted;
        public bool IsFire;
        public bool IsHeal;
        public bool IsInformation;
        public Cell FireCell;
        public Cell HealCell;
        public InformationInfo Information;
    }

    public struct InformationInfo
    {
        public Cell TargetCell;
        public long Information;
    }
}