﻿namespace SwarmBattle.Core
{
    public struct MoveRealInfo
    {
        /// <summary> Entity in coordinates before move </summary>
        public readonly CellEntity Entity; 
        public readonly int Dx;
        public readonly int Dy;

        public MoveRealInfo(CellEntity entity, int dx, int dy)
        {
            Entity = entity;
            Dx = dx;
            Dy = dy;
        }
    }
}