﻿namespace SwarmBattle.Core
{
    public struct EntityPair
    {
        public readonly CellEntity Source;
        public readonly CellEntity Target;

        public EntityPair(CellEntity source, CellEntity target)
        {
            Source = source;
            Target = target;
        }
    }
}