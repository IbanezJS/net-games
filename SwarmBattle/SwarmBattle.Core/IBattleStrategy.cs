﻿using System.Collections.Generic;

namespace SwarmBattle.Core
{
    public interface IBattleStrategy
    {
        /// <summary>
        /// DO NOT USE IT !-_-!
        /// </summary>
        void SetCheatingWorld(BattleWorld world, BattleEntity me);
        void SetData(EntitySettings settings);
        MoveInfo MakeMove(CellEntity meSnap, IEnumerable<CellInfo> myVision);
        /// <summary>
        /// Called after applying move. TotalWin = isSwarmWin.HasValue && isSwarmWin. TotalLoss = isSwarmWin.HasValue && !isSwarmWin
        /// </summary>
        void AfterMove(bool isDead, bool? isSwarmWin);
        void InformationReceived(long information);
    }


}
