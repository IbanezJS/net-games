﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SwarmBattle.Core
{
    public static class Helper
    {
        public static void ConvertDirection(Direction dir, out int dx, out int dy)
        {
            dx = dy = 0;
            switch (dir)
            {
                case Direction.None:
                    break;
                case Direction.Up:
                    dy = 1;
                    break;
                case Direction.Down:
                    dy = -1;
                    break;
                case Direction.Left:
                    dx = -1;
                    break;
                case Direction.Right:
                    dx = 1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
            }
        }

        public static void DoRandomPermutation<T>(IList<T> items, Random rnd, int startIndex, int count)
        {
            for (int i = 0; i < count - 1; ++i)
            {
                var rndIndex = startIndex + rnd.Next(i, count);
                int index = startIndex + i;
                var temp = items[index];
                items[index] = items[rndIndex];
                items[rndIndex] = temp;
            }
        }

        public static Dictionary<string, Type> GetStrategyTypes()
        {
            return Assembly.Load("SwarmBattle.Strategy").GetTypes()
                .Where(t => typeof(IBattleStrategy).IsAssignableFrom(t))
                .Where(t => !t.IsInterface && !t.IsAbstract && t.IsClass)
                .ToDictionary(t => t.Name, t => t);
        }
    }
}
