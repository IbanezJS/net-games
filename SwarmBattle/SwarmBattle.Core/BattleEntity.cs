﻿using System;
using System.Diagnostics;

namespace SwarmBattle.Core
{
    public class BattleEntity
    {
        private readonly BattleWorld world;
        private readonly EntitySettings settings;
        public readonly object LockObj = new object();

        internal IBattleStrategy Strategy { get; }

        public BattleEntity(
            BattleWorld world, 
            IBattleStrategy strategy, 
            int id,
            int swarmId,
            Cell startCell,
            EntitySettings settings)
        {
            Strategy = strategy;
            this.settings = settings;
            Id = id;
            SwarmId = swarmId;
            this.world = world;
            Position = startCell;
            Health = 1;
            Power = 1;
            Mana = 1;
            strategy.SetData(settings);
        }
        
        public int Id { get; }
        public int SwarmId { get; }
        public Cell Position { get; internal set; }
        /// <summary> (0, 1] 0 = death </summary>
        public double Health { get; internal set; }
        /// <summary> [0, 1] Can fire only with power = 1 </summary>
        public double Power { get; internal set; }
        /// <summary> [0, 1] Can heal only with mana = 1 </summary>
        public double Mana { get; internal set; }
        public bool CanFire => Power >= 1 - double.Epsilon;
        public bool CanHeal => Mana >= 1 - double.Epsilon;
        public bool CanBoost => Power >= settings.BoostPowerPenalty;
        public bool CanSendInformation => Power >= settings.InformationPowerPenalty;

        public bool IsEnemyFireable(BattleEntity enemy)
        {
            return CanFire
                   && enemy.Id != Id
                   && enemy.SwarmId != SwarmId
                   && enemy.Position.Distance(Position.X, Position.Y) <= settings.FireVision;
        }

        public bool IsFriendHealable(BattleEntity friend)
        {
            return CanHeal
                   && friend.Id != Id
                   && friend.SwarmId == SwarmId
                   && friend.Position.Distance(Position.X, Position.Y) <= settings.HealVision;
        }

        public bool IsFriendInform(BattleEntity friend)
        {
            return CanSendInformation
                   && friend.Id != Id
                   && friend.SwarmId == SwarmId
                   && friend.Position.Distance(Position.X, Position.Y) <= settings.Vision;
        }

        public bool CanMove(Direction direction, bool isBoosted)
        {
            if (direction == Direction.None)
                return true;
            Helper.ConvertDirection(direction, out int dx, out int dy);
            return world.CanMoveToCell(Position.X + dx, Position.Y + dy) && (!isBoosted || world.CanMoveToCell(Position.X + dx * 2, Position.Y + dy * 2));
        }

        internal MoveInfo MakeMove()
        {
            try
            {
                return Strategy.MakeMove(Clone(), world.GetVision(Position));
            }
            catch (Exception e)
            {
                throw new InvalidOperationException($"ERROR in strategy '{Strategy.GetType().Name}->MakeMove()' : {e}");
            }
        }

        internal void OnMoveEnd(bool isDead, bool? isSwarmWin)
        {
            try
            {
                Strategy.AfterMove(isDead, isSwarmWin);
            }
            catch (Exception e)
            {
                throw new InvalidOperationException($"ERROR in strategy '{Strategy.GetType().Name}->AfterMove()' : {e.Message}");
            }
        }

        internal void SetInformation(long information)
        {
            try
            {
                Strategy.InformationReceived(information);
            }
            catch (Exception e)
            {
                throw new InvalidOperationException($"ERROR in strategy {Strategy.GetType().Name} 'AfterMove' : {e.Message}");
            }
        }

        public CellEntity Clone()
        {
            return new CellEntity(this, settings, Id, SwarmId, Position, Health, Power, Mana);
        }
    }
}