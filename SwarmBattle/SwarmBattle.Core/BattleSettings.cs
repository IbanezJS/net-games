﻿using System.Collections.Generic;

namespace SwarmBattle.Core
{
    public class BattleSettings
    {
        public int Width { get; set; } = 40;
        public int Height { get; set; } = 25;
        public int EntityCount { get; set; } = 30;
        public bool AllowFire { get; set; } = true;
        public bool AllowHeal { get; set; } = true;
        public bool InfinitePower { get; set; } = false;
        public bool InfiniteMana { get; set; } = false;
        public int ThreadCount { get; set; } = -1; //all cpu
        public EntitySettings EntitySettings { get; set; } = new EntitySettings();
        /// <summary> battle strategy type per swarm </summary>
        public List<string> SwormStrategies { get; set; } = new List<string>();
    }

    public class EntitySettings
    {
        public int Vision { get; set; } = 6;
        public int FireVision { get; set; } = 4;
        public int HealVision { get; set; } = 3;

        /// <summary>
        /// (0, 1] how many energy restored after single move
        /// </summary>
        public double PowerRestorePerMove { get; set; } = 0.05;

        /// <summary>
        /// (0, 1] how many Mana restored after single move
        /// </summary>
        public double ManaRestorePerMove { get; set; } = 0.05;

        /// <summary>
        /// [0, 1) - power loss if boosting move
        /// </summary>
        public double BoostPowerPenalty { get; set; } = 0.2;
        /// <summary>
        /// [0, 1) - power loss if send information
        /// </summary>
        public double InformationPowerPenalty { get; set; } = 0.2;
        /// <summary>
        /// (0, 1] - health loss after get shooted
        /// </summary>
        public double FireDamage { get; set; } = 0.3;

        /// <summary>
        /// (0, 1] - health increase after get healed
        /// </summary>
        public double HealFactor { get; set; } = 0.2;

        internal EntitySettings Clone()
        {
            return new EntitySettings
            {
                Vision = Vision,
                FireVision = FireVision,
                HealVision = HealVision,
                PowerRestorePerMove = PowerRestorePerMove,
                ManaRestorePerMove = ManaRestorePerMove,
                BoostPowerPenalty = BoostPowerPenalty,
                FireDamage = FireDamage,
                HealFactor = HealFactor,
                InformationPowerPenalty = InformationPowerPenalty
            };
        }
    }
}