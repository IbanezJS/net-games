﻿namespace SwarmBattle.Core
{
    public struct CellInfo
    {
        public Cell Cell;
        public bool IsEmpty;
        public bool IsEnemy;
        public bool IsFriend;
        public CellEntity Entity;
    }
}