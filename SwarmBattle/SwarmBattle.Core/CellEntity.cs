﻿using System;
using System.Diagnostics;

namespace SwarmBattle.Core
{
    public struct CellEntity : IEquatable<CellEntity>
    {
        private readonly BattleEntity entityInner;
        private readonly EntitySettings settings;

        public CellEntity(
            BattleEntity entityInner,
            EntitySettings settings,
            int id,
            int swarmId,
            Cell currentCell,
            double health,
            double power,
            double mana)
        {
            this.entityInner = entityInner;
            this.settings = settings;
            Id = id;
            SwarmId = swarmId;
            Position = currentCell;
            Health = health;
            Power = power;
            Mana = mana;
        }

        public int Id { get; }
        public int SwarmId { get; }
        public Cell Position { get; }
        /// <summary> (0, 1] 0 = death </summary>
        public double Health { get; }
        /// <summary> [0, 1] Can fire only with power = 1 </summary>
        public double Power { get; }
        /// <summary> [0, 1] Can heal only with mana = 1 </summary>
        public double Mana { get; }
        public bool CanFire => Power >= 1 - double.Epsilon;
        public bool CanHeal => Mana >= 1 - double.Epsilon;
        public bool CanSendInformation => Power >= settings.InformationPowerPenalty;

        public bool IsEnemyFireable(CellEntity enemy)
        {
            return CanFire
                   && enemy.Id != Id
                   && enemy.SwarmId != SwarmId
                   && enemy.Position.Distance(Position.X, Position.Y) <= settings.FireVision;
        }

        public bool IsFriendHealable(CellEntity friend)
        {
            return CanHeal
                   && friend.SwarmId == SwarmId
                   && friend.Id != Id
                   && friend.Position.Distance(Position.X, Position.Y) <= settings.HealVision;
        }

        public bool IsSeeCell(Cell cell)
        {
            return cell.Distance(Position.X, Position.Y) <= settings.Vision;
        }

        public bool IsFriendInform(CellEntity friend)
        {
            return CanSendInformation
                   && friend.Id != Id
                   && friend.SwarmId == SwarmId
                   && friend.Position.Distance(Position.X, Position.Y) <= settings.Vision;
        }

        public bool CanMove(Direction direction, bool isBoosted)
        {
            return entityInner.CanMove(direction, isBoosted);
        }

        public bool Equals(CellEntity other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            return obj is CellEntity other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}