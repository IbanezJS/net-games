﻿using System;
using System.Collections.Generic;
using System.Linq;
using SwarmBattle.Core;

namespace SwarmBattle.Strategy
{
    public abstract class StrategyBase : IBattleStrategy
    {
        protected EntitySettings Settings;
        protected CellEntity Me;
        protected readonly Random Rnd = new Random(Guid.NewGuid().GetHashCode());
        
        protected readonly List<CellEntity> SwarmVision = new List<CellEntity>();
        protected readonly List<CellEntity> EnemyVision = new List<CellEntity>();
        protected readonly Direction[] DirsTmp = { Direction.Left, Direction.Right, Direction.Up, Direction.Down };
        private readonly List<KeyValuePair<double, Cell>> fireTargetsTmp = new List<KeyValuePair<double, Cell>>();

        //centre of the explored world
        protected int CurrentMinX = int.MaxValue;
        protected int CurrentMinY = int.MaxValue;
        protected int CurrentMaxX = int.MinValue;
        protected int CurrentMaxY = int.MinValue;
        
        //information data
        protected readonly Dictionary<int, int> InformationSendCounterMap = new Dictionary<int, int>();
        private readonly Dictionary<int, int> idsTmp = new Dictionary<int, int>();
        
        public virtual void SetCheatingWorld(BattleWorld world, BattleEntity me)
        {
            // DO NOT USE IT
        }

        public virtual void SetData(EntitySettings settings)
        {
            Settings = settings;
            Init();
        }

        protected virtual void Init()
        {
        }

        public virtual void InformationReceived(long information)
        {
        }

        public virtual MoveInfo MakeMove(CellEntity meSnap, IEnumerable<CellInfo> myVision)
        {
            Me = meSnap;
            EnemyVision.Clear();
            SwarmVision.Clear();
            idsTmp.Clear();

            SwarmVision.Add(Me);

            if (meSnap.Position.X - Settings.Vision < CurrentMinX)
                CurrentMinX = meSnap.Position.X - Settings.Vision;
            if (meSnap.Position.X + Settings.Vision > CurrentMaxX)
                CurrentMaxX = meSnap.Position.X + Settings.Vision;
            if (meSnap.Position.Y - Settings.Vision < CurrentMinY)
                CurrentMinY = meSnap.Position.Y - Settings.Vision;
            if (meSnap.Position.Y + Settings.Vision > CurrentMaxY)
                CurrentMaxY = meSnap.Position.Y + Settings.Vision;

            foreach (var kvp in InformationSendCounterMap)
            {
                idsTmp[kvp.Key] = kvp.Value - 1;
            }
            foreach (var kvp in idsTmp)
            {
                if (kvp.Value <= 0)
                    InformationSendCounterMap.Remove(kvp.Key);
                else
                    InformationSendCounterMap[kvp.Key] = kvp.Value;
            }
            
            foreach (var info in myVision)
            {
                if (info.IsFriend)
                    SwarmVision.Add(info.Entity);

                if (info.IsEnemy)
                    EnemyVision.Add(info.Entity);
            }

            bool isFire = CalcFireCell(out var fireCell);
            bool isHeal = CalcHealCell(out var healCell);
            
            var dir = CalcDirection(isFire, isHeal, out bool isBoosted, out bool isInformation, out var informationInfo);

            return new MoveInfo
            {
                Direction = dir,
                IsBoosted = isBoosted,
                IsFire = isFire,
                IsHeal = isHeal,
                IsInformation = isInformation,
                FireCell = fireCell,
                HealCell = healCell,
                Information = informationInfo
            };
        }

        public virtual void AfterMove(bool isDead, bool? isSwarmWin)
        {
        }

        protected Direction GetRandomDirection(CellEntity? me)
        {
            Core.Helper.DoRandomPermutation(DirsTmp, Rnd, 0, DirsTmp.Length);
            if (me == null)
                return DirsTmp[0];
            foreach (var dir in DirsTmp)
            {
                if (me.Value.CanMove(dir, false))
                    return dir;
            }
            return Direction.None;
        }

        protected abstract Direction CalcDirection(bool isFire, bool isHeal, out bool isBoosted, out bool isInformation, out InformationInfo informationInfo);
        
        protected virtual bool CalcFireCell(out Cell fireCell)
        {
            fireCell = default;
            if (!Me.CanFire)
                return false;
            double minHealth = double.MaxValue;
            foreach (var enemy in EnemyVision)
            {
                if (!Me.IsEnemyFireable(enemy))
                    continue;
                double virtualHealth = enemy.Health - Settings.FireDamage;
                if (virtualHealth < minHealth)
                {
                    minHealth = virtualHealth;
                    fireCell = enemy.Position;
                }
            }
            return minHealth < double.MaxValue;
        }

        protected virtual bool CalcHealCell(out Cell healCell)
        {
            healCell = default;
            if (!Me.CanHeal || SwarmVision.Count == 1) //low Mana or no friends around
                return false;

            var healableEnum = SwarmVision.Where(e => !e.Position.Equals(Me.Position) && Me.IsFriendHealable(e) && e.Health < 1);

            double minHealth = double.MaxValue;

            foreach (var entity in healableEnum)
            {
                if (entity.Health < minHealth)
                {
                    minHealth = entity.Health;
                    healCell = entity.Position;
                }
            }

            return minHealth < double.MaxValue;
        }

        protected bool CalcFireCellRnd(out Cell fireCell)
        {
            fireCell = new Cell();
            if (!Me.CanFire)
                return false;
            fireTargetsTmp.Clear();
            double minHealth = double.MaxValue;
            double damageSum = 0;
            foreach (var enemy in EnemyVision)
            {
                if (!Me.IsEnemyFireable(enemy))
                    continue;
                double virtualHealth = enemy.Health;
                foreach (var swarmEntity in SwarmVision)
                {
                    if (swarmEntity.IsEnemyFireable(enemy))
                        virtualHealth -= Settings.FireDamage;
                }
                double damageFactor = 1 - virtualHealth;
                damageSum += damageFactor;
                fireTargetsTmp.Add(new KeyValuePair<double, Cell>(damageFactor, enemy.Position));
                if (virtualHealth < minHealth)
                {
                    minHealth = virtualHealth;
                    fireCell = enemy.Position;
                }
            }

            foreach (var kvp in fireTargetsTmp)
            {
                var damage = kvp.Key;
                var cell = kvp.Value;
                if (Rnd.NextDouble() < damage / damageSum)
                {
                    fireCell = cell;
                    return true;
                }
            }

            return fireTargetsTmp.Count > 0;
        }

        protected bool CalcHealCellRnd(out Cell healCell)
        {
            healCell = new Cell();
            if (!Me.CanHeal || SwarmVision.Count == 1) //low Mana or no friends around
                return false;

            var healableEnum = SwarmVision.Where(e => e.Id != Me.Id && Me.IsFriendHealable(e) && e.Health < 1);

            foreach (var entity in healableEnum)
            {
                double factor = 1;
                foreach (var ent in SwarmVision)
                {
                    if (ent.Id == Me.Id || ent.Id == entity.Id || !ent.IsFriendHealable(entity))
                        continue;
                    factor += 1;
                }

                if (Rnd.NextDouble() < (1 - entity.Health) / factor)
                {
                    healCell = entity.Position;
                    return true;
                }
            }

            return false;
        }
    }
}
