﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SwarmBattle.Core;

namespace SwarmBattle.Strategy.Other
{
    public class Gravity : IBattleStrategy
    {
        private const bool UseBoost = true;
        private const double InitVelocity = 4;
        private const bool EntityRandomVelocity = true;
        private const double MaxVelocity = 8;
        private const double GravityPowerFactor = 0.7;
        
        private BattleWorld world;
        private BattleEntity me;
        private readonly Random rnd = new Random(Guid.NewGuid().GetHashCode());
        private double lastVx;
        private double lastVy;
        private double targetX;
        private double targetY;
        private int maxPlanetRadius;

        private static volatile List<Gravity> allParticles = new List<Gravity>();
        
        public void SetCheatingWorld(BattleWorld world, BattleEntity me)
        {
            //CHEAT for universe simulation
            this.world = world;
            this.me = me;
            maxPlanetRadius = (int)Math.Sqrt(world.Settings.EntityCount);
            world.Settings.InfinitePower = true;
            targetX = me.Position.X;
            targetY = me.Position.Y;

            if (allParticles.Count + 1 > world.Swarms[me.SwarmId].Count)
                allParticles = new List<Gravity>(); //static field magic
            allParticles.Add(this);

            if (EntityRandomVelocity)
            {
                lastVx = (rnd.NextDouble() - 0.5) * 2 * InitVelocity;
                lastVy = (rnd.NextDouble() - 0.5) * 2 * InitVelocity;
            }
        }

        public void SetData(EntitySettings settings)
        {
        }

        public MoveInfo MakeMove(CellEntity meSnap, IEnumerable<CellInfo> myVision)
        {
            var dir = Helper.ConvertVectorToDirection(me.Position.X, me.Position.Y, targetX, targetY);
            var dxAbs = Math.Abs(targetX - me.Position.X);
            var dyAbs = Math.Abs(targetY - me.Position.Y);
            bool isBoosted = UseBoost && (dxAbs >= 2 || dyAbs >= 2);
            
            bool canMove;
            if (isBoosted)
            {
                canMove = me.CanMove(dir, true);
                if (!canMove)
                {
                    isBoosted = false;
                    canMove = me.CanMove(dir, false);
                }
            }
            else
                canMove = me.CanMove(dir, false);

            bool collision = dir != Direction.None && !canMove;

            if (collision)
            {
                Debug.Assert(dir != Direction.None);
                switch (dir)
                {
                    case Direction.Up:
                    case Direction.Down:
                        lastVy = 0;
                        targetY = me.Position.Y;
                        break;
                    case Direction.Left:
                    case Direction.Right:
                        lastVx = 0;
                        targetX = me.Position.X;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                dir = Direction.None;
            }

            if (dir == Direction.None)
                CalcNextStep();
            
            return new MoveInfo {Direction = dir, IsBoosted = isBoosted};
        }

        private bool IsTrap()
        {
            VelocityToDirections(out var dirX, out var dirY);
            return world.IsTrap(me, dirX, dirY);
        }

        private void VelocityToDirections(out Direction dirX, out Direction dirY)
        {
            dirX = Direction.None;
            dirY = Direction.None;
            if (lastVx > 0)
                dirX = Direction.Right;
            else if (lastVx < 0)
                dirX = Direction.Left;
            if (lastVy > 0)
                dirY = Direction.Up;
            else if (lastVy < 0)
                dirY = Direction.Down;
        }

        private void CalcNextStep()
        {
            double sumFx = 0;
            double sumFy = 0;
            foreach (var particle in allParticles)
            {
                if (particle == this)
                    continue;
                var r = me.Position.Distance(particle.me.Position);
                if (r <= 3)
                    continue; //as single particle
                if (r < maxPlanetRadius)
                {
                    if (particle.IsTrap())
                        continue; // single planet moving
                }
                var gravityFactor = GravityPowerFactor / (r * r * r);
                var dx = particle.me.Position.X - me.Position.X;
                var dy = particle.me.Position.Y - me.Position.Y;
                var fx = dx * gravityFactor; // dx/r = cos(f1)
                var fy = dy * gravityFactor; // dy/r = cos(f2)
                sumFx += fx;
                sumFy += fy;
            }

            //F=ma=a (m=1)
            //dv = a*dt = a (dt=1)
            //ds = v*dt = v (dt=1)
            lastVx += sumFx;
            lastVy += sumFy;
            if (Math.Abs(lastVx) > MaxVelocity)
                lastVx = Math.Sign(lastVx) * MaxVelocity;
            if (Math.Abs(lastVy) > MaxVelocity)
                lastVy = Math.Sign(lastVy) * MaxVelocity;
            targetX += lastVx;
            targetY += lastVy;
        }

        public void AfterMove(bool isDead, bool? isSwarmWin)
        {
        }

        public void InformationReceived(long information)
        {
        }
    }
}
