﻿using System;
using SwarmBattle.Core;

namespace SwarmBattle.Strategy
{
    public static class Helper
    {
        public static Direction ConvertVectorToDirection(double x1, double y1, double x2, double y2)
        {
            double dx = x2 - x1;
            double dy = y2 - y1;
            double dxAbs = Math.Abs(dx);
            double dyAbs = Math.Abs(dy);
            if (dxAbs <= 0.5 && dyAbs <= 0.5)
                return Direction.None;
            if (dxAbs >= dyAbs)
                return dx >= 0 ? Direction.Right : Direction.Left;
            return dy >= 0 ? Direction.Up : Direction.Down;
        }

        public static Direction ConvertVectorToDirection(Cell from, Cell to)
        {
            return ConvertVectorToDirection(from.X, from.Y, to.X, to.Y);
        }
    }
}
