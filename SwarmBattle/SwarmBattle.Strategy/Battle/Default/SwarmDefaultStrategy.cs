﻿using System;
using SwarmBattle.Core;

namespace SwarmBattle.Strategy.Battle.Default
{
    public class SwarmDefaultStrategy : StrategyBase
    {
        //centre of the explored world
        private double currentCentreShiftX;
        private double currentCentreShiftY;

        //spiral move params
        private double currentAngle; //radians
        private double currentRadius = MinRadius;
        private double currentMaxRadius = 4*MinRadius;
        private double currentRadiusDirection = 1;
        private double circleDistancePerMove;

        //information data
        private int informationReceiveCounter;
        private int informationLife;
        private double informationScore;
        private Cell informationWeakCell;
        private double informationWeakHealth;
        private bool resendFlag;

        //todo magic const numbers
        private const double MinRadius = 3;
        private const double CircleDistancePerMove = 1;
        private const double MaxCircleDistanceMutate = 0.3;
        private const bool UseSecondStrategyIfNotFirst = true;
        private const bool UseRandomIfNotStrategy = true;

        protected override void Init()
        {
            informationLife = Settings.Vision * 2;
            circleDistancePerMove = CircleDistancePerMove + (Rnd.NextDouble() - 0.5) * 2 * MaxCircleDistanceMutate;
        }

        public override void InformationReceived(long information)
        {
            long scoreLong = information & 0xffffL;
            double score = scoreLong * 0.01;
            if (score > informationScore)
            {
                information >>= 16;
                int x = (int)(information & 0xffffL);
                information >>= 16;
                int y = (int)(information & 0xffffL);
                var weakCell = new Cell(x, y);
                information >>= 16;
                int weakHealthInt = (int)(information & 0xffffL);
                double weakHealth = weakHealthInt * 0.01;

                if (informationScore > 0)
                    resendFlag = true;

                informationReceiveCounter = informationLife;
                informationScore = score;
                informationWeakCell = weakCell;
                informationWeakHealth = weakHealth;
            }
        }

        protected override Direction CalcDirection(bool isFire, bool isHeal, out bool isBoosted, out bool isInformation, out InformationInfo informationInfo)
        {
            if (--informationReceiveCounter <= 0)
            {
                informationWeakCell = default;
                informationWeakHealth = 0;
                informationScore = 0;
                resendFlag = false;
            }

            var dir = CalcDirectionMain(); //important: must call every time to calc main movement

            if (UseSecondStrategyIfNotFirst && (!Me.CanMove(dir, false) || Rnd.NextDouble() < 0.1))
                dir = CalcDirectionSecond(); //to local centre mass (me not included)

            bool trySendInformation = false;
            Cell weakCell = default;
            double weakHealth = 0;
            double score = 0;

            if (NeedToRunAway(out Direction runDirection, out isBoosted))
                dir = runDirection;
            else if (!isFire && CalcWeakEnemy(out Direction weakDirection, out score, out var weakTarget) && score > informationScore)
            {
                dir = weakDirection;
                trySendInformation = true;
                weakCell = weakTarget.Position;
                weakHealth = weakTarget.Health;
            }
            else if (informationScore > 0)
            {
                dir = Helper.ConvertVectorToDirection(Me.Position, informationWeakCell);
                if (resendFlag)
                {
                    trySendInformation = true;
                    resendFlag = false;
                }
                else if (Rnd.NextDouble() < 0.1)
                    trySendInformation = true;
                weakCell = informationWeakCell;
                weakHealth = informationWeakHealth;
                score = informationScore;
            }

            if (UseRandomIfNotStrategy && (dir == Direction.None || !Me.CanMove(dir, isBoosted)))
                dir = GetRandomDirection(Me);

            isInformation = false;
            informationInfo = default;
            if (!isFire && trySendInformation && CalcSendInformation(weakCell, weakHealth, out CellEntity infoTarget))
            {
                isInformation = true;
                informationInfo = SendInformation(infoTarget, score, weakHealth, weakCell);
            }

            return dir;
        }

        private Direction CalcDirectionMain()
        {
            var centreX = Me.Position.X;
            var centreY = Me.Position.Y;

            double worldVisionCentreX = (CurrentMinX + CurrentMaxX) * 0.5;
            double worldVisionCentreY = (CurrentMinY + CurrentMaxY) * 0.5;

            double deltaAngle = circleDistancePerMove / currentRadius;
            currentAngle += deltaAngle;

            double newVirtualCentreX = worldVisionCentreX + currentCentreShiftX + currentRadius * Math.Cos(currentAngle);
            double newVirtualCentreY = worldVisionCentreY + currentCentreShiftY + currentRadius * Math.Sin(currentAngle);

            if (currentAngle > 2 * Math.PI) //start new circle
            {
                currentAngle = 0;
                currentRadius += currentRadiusDirection * MinRadius;
                
                if (currentRadius > currentMaxRadius)
                {
                    currentMaxRadius = Math.Max((CurrentMaxX - CurrentMinX) / 2.0, (CurrentMaxY - CurrentMinY) / 2.0) + 4*Settings.Vision; 
                    //currentRadius = currentMaxRadius;
                    currentRadiusDirection = -1;
                }
                else if (currentRadius <= 2*MinRadius) 
                    currentRadiusDirection = 1;

                //mutate global centre
                currentCentreShiftX = (Rnd.NextDouble() - 0.5) * 2 * Settings.Vision;
                currentCentreShiftY = (Rnd.NextDouble() - 0.5) * 2 * Settings.Vision;
            }

            return Helper.ConvertVectorToDirection(centreX, centreY, newVirtualCentreX, newVirtualCentreY);
        }

        private Direction CalcDirectionSecond()
        {
            if (SwarmVision.Count == 1) //only me and no one around
                return Direction.None;
            
            CalcCentreMass(false, out var centreX, out var centreY);
            
            return Helper.ConvertVectorToDirection(Me.Position.X, Me.Position.Y, centreX, centreY);
        }

        private void CalcCentreMass(bool includeMe, out double x, out double y)
        {
            x = 0;
            y = 0;
            int count = 0;
            foreach (var entity in SwarmVision)
            {
                if (entity.Position.Equals(Me.Position) && !includeMe)
                    continue;
                ++count;
                x += entity.Position.X;
                y += entity.Position.Y;
            }
            x /= count;
            y /= count;
        }

        protected override bool CalcFireCell(out Cell fireCell)
        {
            return CalcFireCellRnd(out fireCell);
        }

        protected override bool CalcHealCell(out Cell healCell)
        {
            return CalcHealCellRnd(out healCell);
        }

        private bool NeedToRunAway(out Direction direction, out bool isBoosted)
        {
            direction = Direction.None;
            isBoosted = false;
            if (EnemyVision.Count == 0)
                return false;
            if (EnemyVision.Count == 1 && EnemyVision[0].Health <= Me.Health && EnemyVision[0].Power <= Me.Power)
                return false;

            double healthVirtual = CalcMyVirtualHealth();

            if (Me.CanFire && healthVirtual > Settings.FireDamage)
                return false; //we can stand all enemies )) and shoot back

            //run away
            double maxDistance = CalcPowerWeightedEnemyDistance(Me.Position);
            foreach (var dir in DirsTmp)
            {
                Core.Helper.ConvertDirection(dir, out int dx, out int dy);
                if (healthVirtual <= 0.5) //boost only when low potential health
                {
                    double distance1 = CalcPowerWeightedEnemyDistance(new Cell(Me.Position.X + dx * 2, Me.Position.Y + dy * 2)); //*2 because boosted
                    if (Me.CanMove(dir, true) && distance1 > maxDistance)
                    {
                        maxDistance = distance1;
                        direction = dir;
                        isBoosted = true;
                    }
                }
                double distance2 = CalcPowerWeightedEnemyDistance(new Cell(Me.Position.X + dx, Me.Position.Y + dy));
                if (Me.CanMove(dir, false) &&  distance2 > maxDistance)
                {
                    maxDistance = distance2;
                    direction = dir;
                    isBoosted = false;
                }
            }

            return direction != Direction.None;
        }

        private double CalcMyVirtualHealth()
        {
            double healthVirtual = Me.Health;
            foreach (var entity in EnemyVision)
            {
                double distance = Me.Position.Distance(entity.Position);
                double powerСhase = (Settings.Vision - distance + 2) * Settings.PowerRestorePerMove;
                if (entity.Power <= 1 - powerСhase)
                    continue; //low power. we can run away later
                healthVirtual -= Settings.FireDamage;
            }
            return healthVirtual;
        }

        private double CalcPowerWeightedEnemyDistance(Cell meVirtual)
        {
            double distanceSum = 0;
            double powerSum = 0;
            foreach (var entity in EnemyVision)
            {
                double distance = meVirtual.Distance(entity.Position);
                distanceSum += distance * entity.Power;
                powerSum += entity.Power;
            }
            if (powerSum > 0)
                return distanceSum / powerSum;
            return 0;
        }

        private bool CalcWeakEnemy(out Direction direction, out double score, out CellEntity target)
        {
            //if we are here then we don't run away and not fire yet
            direction = Direction.None;
            score = 0;
            target = default;

            double maxScore = double.MinValue;

            foreach (var enemy in EnemyVision)
            {
                double ourPower = 0;
                double ourHealth = 0;
                double ourMana = 0;
                foreach (var friend in SwarmVision)
                {
                    if (!friend.IsSeeCell(enemy.Position))
                        continue;
                    ourPower += friend.Power;
                    ourHealth += friend.Health;
                    ourMana += friend.Mana;
                }
                double enemyPower = 0;
                double enemyHealth = 0;
                double enemyMana = 0;
                foreach (var enemy2 in EnemyVision)
                {
                    if (!enemy2.IsSeeCell(enemy.Position))
                        continue;
                    enemyPower += enemy2.Power;
                    enemyHealth += enemy2.Health;
                    enemyMana += enemy2.Mana;
                }

                if (ourPower >= enemyPower && ourHealth >= enemyHealth && ourMana >= enemyMana)
                {
                    score = (ourPower - enemyPower) + (ourHealth - enemyHealth) + (ourMana - enemyMana);
                    if (score > maxScore)
                    {
                        maxScore = score;
                        direction = Helper.ConvertVectorToDirection(Me.Position, enemy.Position);
                        target = enemy;
                    }
                }
            }
            return direction != Direction.None;
        }

        private bool CalcSendInformation(Cell weakCell, double weakHealth, out CellEntity infoTarget)
        {
            infoTarget = default;
            if (Me.Power < 0.9)
                return false;
            double minDistance = double.MaxValue;
            foreach (var friend in SwarmVision)
            {
                if (friend.Position.Equals(Me.Position) 
                    || friend.IsSeeCell(weakCell) 
                    || friend.Health < weakHealth
                    || InformationSendCounterMap.ContainsKey(friend.Id))
                    continue;
                var dist = friend.Position.Distance(weakCell);
                if (dist < minDistance)
                {
                    minDistance = dist;
                    infoTarget = friend;
                }
            }
            return minDistance < double.MaxValue;
        }

        private InformationInfo SendInformation(CellEntity infoTarget, double score, double weakHealth, Cell weakCell)
        {
            InformationSendCounterMap[infoTarget.Id] = informationLife;
            long scoreLong = (long)(score * 100);
            long healthLong = (long)(weakHealth * 100);
            long information = scoreLong;
            information |= (long)weakCell.X << 16;
            information |= (long)weakCell.Y << 32;
            information |= healthLong << 48;
            return  new InformationInfo{Information = information, TargetCell = infoTarget.Position};
        }
    }
}
