﻿using SwarmBattle.Core;

namespace SwarmBattle.Strategy.Battle.Default
{
    public class Zubastiki : StrategyBase
    {
        private int currentLeadId;
        private Cell? leadPrevCell;
        private int targetMoveCounter;
        private Cell targetCell;

        private int informationLife;
        private int informationLeadReceiveCounter;
        private int informationLeadWhoSendId;
        private int informationLeadId;
        
        protected override void Init()
        {
            informationLife = (int)(Settings.Vision * 1.5);
        }

        public override void InformationReceived(long information)
        {
            int whoSendId = (int)(information & 0xffffL);
            information >>= 16;
            int leadId = (int)(information & 0xffffL);
            if (leadId > informationLeadId)
            {
                informationLeadReceiveCounter = informationLife;
                informationLeadId = leadId;
                informationLeadWhoSendId = whoSendId;
            }
        }

        protected override Direction CalcDirection(
            bool isFire,
            bool isHeal, 
            out bool isBoosted, 
            out bool isInformation,
            out InformationInfo informationInfo)
        {
            isBoosted = false;
            isInformation = false;
            informationInfo = default;

            if (--informationLeadReceiveCounter <= 0)
            {
                informationLeadWhoSendId = 0;
                informationLeadId = 0;
            }

            CellEntity currentLead = default;
            Cell maxLeadPos = default;
            int maxLeadId = 0;
            CellEntity informationLeadEntity = default;
            foreach (var friend in SwarmVision)
            {
                if (friend.Id == Me.Id)
                    continue;
                if (friend.Id > maxLeadId)
                {
                    maxLeadId = friend.Id;
                    maxLeadPos = friend.Position;
                }
                if (friend.Id == currentLeadId)
                    currentLead = friend;
                if (friend.Id == informationLeadWhoSendId)
                    informationLeadEntity = friend;
            }

            if (informationLeadEntity.Id > 0 && informationLeadId > maxLeadId)
            {
                maxLeadId = informationLeadId;
                maxLeadPos = informationLeadEntity.Position;
            }

            Direction dir;
            if (Me.Id > maxLeadId)
            {
                leadPrevCell = null;
                dir = GetDirectionMeLead();
            }
            else
            {
                if (currentLead.Id != maxLeadId)
                    leadPrevCell = null;
                if (currentLead.Id == maxLeadId && leadPrevCell.HasValue)
                    dir = Helper.ConvertVectorToDirection(leadPrevCell.Value, currentLead.Position);
                else
                    dir = Helper.ConvertVectorToDirection(Me.Position, maxLeadPos);

                var dist = Me.Position.Distance(maxLeadPos);
                if (dist < 3)
                    dir = GetOutDirection(maxLeadPos);
                
                currentLeadId = maxLeadId;
                leadPrevCell = maxLeadPos;

                if (!isFire && CalcSendLeadInformation(maxLeadId, maxLeadPos, out var infoTarget))
                {
                    isInformation = true;
                    informationInfo = SendLeadInformation(infoTarget, maxLeadId);
                }
            }

            return dir;
        }

        private bool CalcSendLeadInformation(int leadId, Cell leadCell, out CellEntity infoTarget)
        {
            infoTarget = default;
            if (Me.Power < 0.9)
                return false;
            foreach (var friend in SwarmVision)
            {
                if (friend.Id == Me.Id
                    || friend.Id == leadId
                    || friend.IsSeeCell(leadCell)
                    || InformationSendCounterMap.ContainsKey(friend.Id))
                    continue;
                infoTarget = friend;
                return true;
            }
            return false;
        }

        private InformationInfo SendLeadInformation(CellEntity infoTarget, int leadId)
        {
            InformationSendCounterMap[infoTarget.Id] = informationLife;
            long information = Me.Id; //who send id
            information |= (long)leadId << 16; //max lead we see or information received
            return new InformationInfo { Information = information, TargetCell = infoTarget.Position };
        }

        private Direction GetDirectionMeLead()
        {
            if (--targetMoveCounter <= 0)
            {
                var targetX = Rnd.Next(CurrentMinX, CurrentMaxX);
                var targetY = Rnd.Next(CurrentMinY, CurrentMaxY);
                if (Rnd.NextDouble() < 0.6)
                {
                    int index = Rnd.Next(0, 4);
                    switch (index)
                    {
                        case 0:
                            targetX = CurrentMinX;
                            targetY = CurrentMinY;
                            break;
                        case 1:
                            targetX = CurrentMaxX;
                            targetY = CurrentMinY;
                            break;
                        case 2:
                            targetX = CurrentMaxX;
                            targetY = CurrentMaxY;
                            break;
                        case 3:
                            targetX = CurrentMinX;
                            targetY = CurrentMaxY;
                            break;
                    }
                }
                targetCell = new Cell(targetX, targetY);
                targetMoveCounter = (int) Me.Position.Distance(targetCell);
            }
            var dir = Helper.ConvertVectorToDirection(Me.Position, targetCell);
            if (!Me.CanMove(dir, false))
                dir = GetRandomDirection(Me);
            return dir;
        }

        private Direction GetOutDirection(Cell from)
        {
            var fromDirection = Helper.ConvertVectorToDirection(from, Me.Position);
            if (Me.CanMove(fromDirection, false))
                return fromDirection;
            var toDirection = Helper.ConvertVectorToDirection(Me.Position, from);
            for (int i = 1; i <= 4; i++)
            {
                var dir = (Direction) i;
                if (dir == fromDirection || dir == toDirection) 
                    continue;
                if (Me.CanMove(dir, false))
                    return dir;
            }
            return Direction.None;
        }

        protected override bool CalcFireCell(out Cell fireCell)
        {
            return CalcFireCellRnd(out fireCell);
        }

        protected override bool CalcHealCell(out Cell healCell)
        {
            return CalcHealCellRnd(out healCell);
        }
    }
}
