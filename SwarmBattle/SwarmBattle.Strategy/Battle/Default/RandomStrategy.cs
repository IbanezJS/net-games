﻿using SwarmBattle.Core;

namespace SwarmBattle.Strategy.Battle.Default
{
    public class RandomStrategy : StrategyBase
    {
        protected override Direction CalcDirection(bool isFire, bool isHeal, out bool isBoosted, out bool isInformation, out InformationInfo informationInfo)
        {
            isBoosted = Rnd.Next(0, 10) == 0; //10% prob
            isInformation = false;
            informationInfo = default;
            return GetRandomDirection(Me);
        }
    }
}