﻿using System.Windows.Media;

namespace SwarmBattle.UI
{
    public class UISettings
    {
        /// <summary> Calculated instantly </summary>
        public double PixelsPerCell { get; set; }
        public double MoveTimeMs { get; set; } = 250;
        public bool DrawVision { get; set; } = false;
        public bool DrawDeath { get; set; } = true;
        public int DeathMoveCount { get; set; } = 3;
        public bool DrawBlood { get; set; } = true;
        public bool DrawProgressBar { get; set; } = true;
        public bool AddByClick { get; set; } = false;
        public bool TrySkipTimeLag { get; set; } = false;
    }
}