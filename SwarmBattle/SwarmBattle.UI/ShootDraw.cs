﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SwarmBattle.UI
{
    public class ShootDraw
    {
        private struct LineInfo
        {
            public int X1;
            public int Y1;
            public int X2;
            public int Y2;
        }

        private readonly UISettings uiSettings;
        private readonly ICoordinateConverter converter;
        private readonly EntityDraw source;
        private readonly EntityDraw target;
        private readonly Color color;
        private readonly bool useRandom;
        private readonly Random rnd;
        private readonly LineInfo[] lines;
        private long sequence;
        private readonly byte alphaDelta;
        
        public ShootDraw(
            ICoordinateConverter converter,
            EntityDraw source, 
            EntityDraw target, 
            Settings settings, 
            Color color, 
            int lineCount,
            bool useRandom)
        {
            uiSettings = settings.UiSettings;
            this.converter = converter;
            this.source = source;
            this.target = target;
            this.color = color;
            lines = new LineInfo[lineCount];
            alphaDelta = (byte)(255 / lineCount);
            this.useRandom = useRandom;
            rnd = new Random(Guid.NewGuid().GetHashCode());
        }

        public void Draw(WriteableBitmap bitmap)
        {
            if (converter.IsSuspended())
                return;
            int alpha = 255;
            long seq = sequence++;
            int index = (int)(seq++ % lines.Length);
            DrawAndStore(bitmap, alpha, index);
            if (sequence < lines.Length)
                return;
            for (int i = 0; i < lines.Length - 1; i++)
            {
                index = (int)(seq++ % lines.Length);
                alpha -= alphaDelta;
                DrawInner(bitmap, alpha, index);
            }
        }

        private void DrawAndStore(WriteableBitmap bitmap, int alpha, int index)
        {
            int x1 = (int)(source.CurrentXpx + uiSettings.PixelsPerCell * 0.5);
            int y1 = (int)(source.CurrentYpx + uiSettings.PixelsPerCell * 0.5);
            int x2 = (int)(target.CurrentXpx + uiSettings.PixelsPerCell * 0.5);
            int y2 = (int)(target.CurrentYpx + uiSettings.PixelsPerCell * 0.5);
            if (useRandom)
            {
                x2 += (int)((rnd.NextDouble() - 0.5) * uiSettings.PixelsPerCell * 0.5);
                y2 += (int)((rnd.NextDouble() - 0.5) * uiSettings.PixelsPerCell * 0.5);
            }
            lines[index] = new LineInfo { X1 = x1, Y1 = y1, X2 = x2, Y2 = y2 };
            
            DrawInner(bitmap, alpha, index);
        }

        private void DrawInner(WriteableBitmap bitmap, int alpha, int index)
        {
            var info = lines[index];
            bitmap.DrawLine(info.X1, info.Y1, info.X2, info.Y2, color.Alpha((byte)alpha));
        }
    }
}