﻿using System;
using System.Diagnostics;
using System.Windows.Media;

namespace SwarmBattle.UI
{
    public static class Helper
    {
        public static Color Alpha(this Color color, byte alpha)
        {
            return Color.FromArgb(alpha, color.R, color.G, color.B);
        }
    }
}
