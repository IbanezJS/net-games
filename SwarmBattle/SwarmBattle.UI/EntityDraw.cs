﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using SwarmBattle.Core;

namespace SwarmBattle.UI
{
    public class EntityDraw
    {
        private readonly ICoordinateConverter converter;
        private CellEntity entitySnapshot;
        private readonly BattleSettings battleSettings;
        private readonly UISettings uiSettings;
        private readonly Color colorTransparent;

        //(x,y) - top left of cell rectangle
        private double xDrawFrom;
        private double yDrawFrom;
        private double xDrawTarget;
        private double yDrawTarget;

        private double timeSnapshotMs;
        private bool shootFlag;
        private bool healFlag;
        private bool informationFlag;
        private byte currentFadeAlpha;
        private double currentHealDeltaPx;
        private double currentInformationDeltaPx;

        private readonly LinkedList<Cell> drawHistory;
        private readonly int maxDrawHistory;
        public bool IsVisible { get; private set; }

        //(x,y) - top left of cell rectangle
        public int CurrentXpx { get; private set; }
        public int CurrentYpx { get; private set; }
        public Color Color { get; }

        public EntityDraw(ICoordinateConverter converter, CellEntity entitySnapshot, Settings settings, Color color, int maxDrawHistory)
        {
            this.converter = converter;
            this.entitySnapshot = entitySnapshot;
            this.maxDrawHistory = maxDrawHistory;
            battleSettings = settings.BattleSettings;
            uiSettings = settings.UiSettings;
            Color = color;
            colorTransparent = Color.FromArgb((byte)(color.A / 3), color.R, color.G, color.B);
            drawHistory = new LinkedList<Cell>();

            SetNextMove(new MoveRealInfo(entitySnapshot, 0, 0), 0);
        }

        public void SetNextMove(MoveRealInfo move, double elapsedMs)
        {
            entitySnapshot = move.Entity;
            var oldCell = entitySnapshot.Position;
            shootFlag = false;
            healFlag = false;
            informationFlag = false;
            timeSnapshotMs = elapsedMs;
            var newCell = new Cell(oldCell.X + move.Dx, oldCell.Y + move.Dy);
            IsVisible = converter.IsCellVisible(oldCell) && converter.IsCellVisible(newCell);
            converter.ConvertCoordinates(oldCell, out xDrawFrom, out yDrawFrom);
            converter.ConvertCoordinates(newCell, out xDrawTarget, out yDrawTarget);
        }

        public void SetShooted()
        {
            shootFlag = true;
        }

        public void SetHealed()
        {
            healFlag = true;
        }

        public void SetInformationReceived()
        {
            informationFlag = true;
        }

        public void SetTime(double elapsedMs)
        {
            double deltaMs = Math.Min(elapsedMs - timeSnapshotMs, uiSettings.MoveTimeMs); 
            double timeFactor = deltaMs / uiSettings.MoveTimeMs;
            double xDelta = timeFactor * (xDrawTarget - xDrawFrom);
            double yDelta = timeFactor * (yDrawTarget - yDrawFrom);
            CurrentXpx = (int) (xDrawFrom + xDelta);
            CurrentYpx = (int) (yDrawFrom + yDelta);
            currentFadeAlpha = (byte)(255 * (1 - timeFactor));
            currentHealDeltaPx = timeFactor * uiSettings.PixelsPerCell * 0.5;
            currentInformationDeltaPx = timeFactor * uiSettings.PixelsPerCell * 0.5;
        }

        public void DrawBackground(WriteableBitmap bitmap)
        {
            if (!IsVisible || converter.IsSuspended())
                return;
            if (uiSettings.DrawVision)
                DrawVision(bitmap);
            if (uiSettings.DrawBlood)
                DrawBlood(bitmap);
            if (healFlag)
                DrawHealAura(bitmap);
            if (informationFlag)
                DrawInformationAura(bitmap);
        }

        public void DrawForground(WriteableBitmap bitmap)
        {
            if (!IsVisible || converter.IsSuspended())
                return;
            DrawEntity(bitmap);
            if (uiSettings.DrawBlood)
                StoreDrawHistory();
        }

        private void DrawBlood(WriteableBitmap bitmap)
        {
            if (drawHistory.Count == 0)
                return;
            int alpha = 255;
            int alphaDelta = (byte)(255.0 / drawHistory.Count);
            double a = uiSettings.PixelsPerCell - 4;
            foreach (var cell in drawHistory)
            {
                Debug.Assert(alpha >= 0);
                int x1 = cell.X + 2;
                int y1 = cell.Y + 2;
                int x2 = (int)(x1 + a);
                int y2 = (int)(y1 + a);
                byte al = (byte) alpha;
                bitmap.FillEllipse(x1, y1, x2, y2, Colors.Red.Alpha(al));
                alpha -= alphaDelta;
            }
        }

        private void DrawHealAura(WriteableBitmap bitmap)
        {
            double a = uiSettings.PixelsPerCell + 2*currentHealDeltaPx;
            int x1 = (int)(CurrentXpx - currentHealDeltaPx);
            int y1 = (int)(CurrentYpx - currentHealDeltaPx);
            int x2 = (int)(x1 + a);
            int y2 = (int)(y1 + a);

            bitmap.FillEllipse(x1, y1, x2, y2, Colors.Magenta.Alpha(currentFadeAlpha));
        }

        private void DrawInformationAura(WriteableBitmap bitmap)
        {
            double a = uiSettings.PixelsPerCell + 2 * currentInformationDeltaPx;
            int x1 = (int)(CurrentXpx - currentInformationDeltaPx);
            int y1 = (int)(CurrentYpx - currentInformationDeltaPx);
            int x2 = (int)(x1 + a);
            int y2 = (int)(y1 + a);

            bitmap.FillEllipse(x1, y1, x2, y2, Colors.YellowGreen.Alpha(currentFadeAlpha));
        }

        private void StoreDrawHistory()
        {
            drawHistory.AddFirst(new Cell(CurrentXpx, CurrentYpx));
            var capacity = (int)((1.0 - entitySnapshot.Health) * maxDrawHistory);
            capacity = Math.Min(maxDrawHistory, Math.Max(0, capacity));
            while (drawHistory.Count > capacity)
            {
                drawHistory.RemoveLast();
            }
        }

        private void DrawEntity(WriteableBitmap bitmap)
        {
            double a = uiSettings.PixelsPerCell;
            int x1 = CurrentXpx;
            int y1 = CurrentYpx;
            int x2 = (int)(x1 + a);
            int y2 = (int)(y1 + a);

            byte alpha = 255;
            if (shootFlag)
            {
                bitmap.FillEllipse(x1, y1, x2, y2, Colors.Red.Alpha(currentFadeAlpha));
                if (currentFadeAlpha > 150)
                    return;
                alpha -= currentFadeAlpha;
            }

            bitmap.FillEllipse(x1, y1, x2, y2, Color.Alpha(alpha));

            double a1 = (0.7071 * a) * 0.9;
            double dxy = (0.1464466 * a) + 0.05*a1 + 1;
            double a1_3 = a1 / 3.0;

            double health = Math.Max(Math.Min(1, entitySnapshot.Health), 0);
            double power = Math.Max(Math.Min(1, entitySnapshot.Power), 0);
            double mana = Math.Max(Math.Min(1, entitySnapshot.Mana), 0);

            int x1_1 = (int)(x1 + dxy);
            int y1_1 = (int)(y1 + dxy);
            int x2_1 = (int)(x1_1 + a1 * health);
            int y2_1 = (int)(y1_1 + a1_3);

            bitmap.FillRectangle(x1_1, y1_1, x2_1, y2_1, Colors.Lime.Alpha(alpha));

            int x1_2 = (int)(x1 + dxy);
            int y1_2 = (int)(y1 + dxy + a1_3);
            int x2_2 = (int)(x1_2 + a1 * power);
            int y2_2 = (int)(y1_2 + a1_3);

            bitmap.FillRectangle(x1_2, y1_2, x2_2, y2_2, Colors.Yellow.Alpha(alpha));

            int x1_3 = (int)(x1 + dxy);
            int y1_3 = (int)(y1 + dxy + 2*a1_3);
            int x2_3 = (int)(x1_3 + a1 * mana);
            int y2_3 = (int)(y1_3 + a1_3);

            bitmap.FillRectangle(x1_3, y1_3, x2_3, y2_3, Colors.Magenta.Alpha(alpha));
        }

        private void DrawVision(WriteableBitmap bitmap)
        {
            DrawCentreCircle(bitmap, battleSettings.EntitySettings.Vision);
            DrawCentreCircle(bitmap, battleSettings.EntitySettings.FireVision);
            DrawCentreCircle(bitmap, battleSettings.EntitySettings.HealVision);
        }

        private void DrawCentreCircle(WriteableBitmap bitmap, int radiusCells)
        {
            int xc = (int)(CurrentXpx + 0.5 * uiSettings.PixelsPerCell);
            int yc = (int)(CurrentYpx + 0.5 * uiSettings.PixelsPerCell);
            int r = (int) (radiusCells * uiSettings.PixelsPerCell);
            bitmap.DrawEllipseCentered(xc, yc, r, r, colorTransparent);
        }
    }
}
