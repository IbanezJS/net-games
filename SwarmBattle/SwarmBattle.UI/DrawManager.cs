﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using SwarmBattle.Core;

namespace SwarmBattle.UI
{
    public interface ICoordinateConverter
    {
        bool IsCellVisible(Cell cell);
        void ConvertCoordinates(Cell cell, out double xPx, out double yPx);
        bool IsSuspended();
    }

    public class DrawManager : ICoordinateConverter
    {
        private static readonly Color[] colors = {Colors.Blue, Colors.Black, Colors.Sienna, Colors.DeepPink, Colors.DarkCyan  }; //todo

        private readonly BattleWorld world;
        private readonly Settings settings;
        private readonly UISettings uiSettings;
        private readonly WriteableBitmap batlleBitmap;
        private readonly int bitmapWidthPx;
        private readonly int initialBitmapHeightPx;
        private int bitmapHeightPx;
        private const int ProgressHeightPx = 66;
        private int progressHeightPx;
        private readonly Color backgroundColor;
        private readonly Dispatcher uiDispatcher;
        private readonly Dictionary<int, EntityDraw> entities; //int = entity.Id
        private readonly List<ShootDraw> interactionPairs;
        private readonly HashSet<DeathDraw> deaths;
        private readonly List<DeathDraw> deathDrawsTmp;
        private double lastMoveVirtualTimeMs;
        private BattleActionInfo lastIterationData;
        private readonly List<CellEntity> lastMoveDeadEntities = new List<CellEntity>();
        private bool isSuspended;
        private bool isEnd;
        private readonly Stopwatch notDrawTimer = new Stopwatch();

        //measure in virtual battle cells
        private int battleStartX;
        private int battleStartY;
        private int battleVisibleWidth;
        private int battleVisibleHeight;

        //zoom
        private int zoomX1;
        private int zoomY1;
        private int zoomX2;
        private int zoomY2;
        private bool isZooming;

        //progress
        private readonly Color[] swarmColors;
        private double[] healthProgress;
        private double[] powerProgress;
        private double[] manaProgress;

        public DrawManager(
            BattleWorld world, 
            Settings settings,
            WriteableBitmap batlleBitmap, 
            int bitmapWidthPx,
            int bitmapHeightPx,
            Color backgroundColor,
            Dispatcher uiDispatcher)
        {
            this.world = world;
            this.settings = settings;
            uiSettings = settings.UiSettings;
            this.batlleBitmap = batlleBitmap;
            progressHeightPx = settings.UiSettings.DrawProgressBar ? ProgressHeightPx : 0;
            this.bitmapWidthPx = bitmapWidthPx;
            initialBitmapHeightPx = bitmapHeightPx;
            this.bitmapHeightPx = initialBitmapHeightPx - progressHeightPx;
            this.backgroundColor = backgroundColor;
            this.uiDispatcher = uiDispatcher;

            ResetGrid(false);

            entities = new Dictionary<int, EntityDraw>(world.Swarms.Select(l => l.Count).Sum());
            interactionPairs = new List<ShootDraw>();
            deaths = new HashSet<DeathDraw>();
            deathDrawsTmp = new List<DeathDraw>();
            var rnd = new Random();
            swarmColors = new Color[world.Swarms.Count];
            healthProgress = new double[world.Swarms.Count];
            powerProgress = new double[world.Swarms.Count];
            manaProgress = new double[world.Swarms.Count];
            for (int i = 0; i < world.Swarms.Count; i++) //todo colors
            {
                var swarmList = world.Swarms[i];
                var color = i < colors.Length ? colors[i] : Color.FromRgb((byte)rnd.Next(0, 255), (byte)rnd.Next(0, 255), (byte)rnd.Next(0, 255));
                swarmColors[i] = color;
                healthProgress[i] = 1;
                powerProgress[i] = 1;
                manaProgress[i] = 1;
                foreach (var entity in swarmList)
                {
                    var entitySnap = entity.Clone(); //important to get snap because calc process is parallel
                    entities.Add(entitySnap.Id, new EntityDraw(this, entitySnap, settings, color, 20));
                }
            }
            world.OnEntityAddManual += WorldOnEntityAddManual;
        }

        private void WorldOnEntityAddManual(CellEntity entity)
        {
            //IMPORTANT: BeginInvoke() to prevent dead lock
            uiDispatcher.BeginInvoke(new Action(() => entities.Add(entity.Id, new EntityDraw(this, entity, settings, swarmColors[entity.SwarmId], 20))));
        }

        public void UpdateDrawProgressSettings()
        {
            progressHeightPx = settings.UiSettings.DrawProgressBar ? ProgressHeightPx : 0;
            bitmapHeightPx = initialBitmapHeightPx - progressHeightPx;
            ResetGrid();
        }

        public void StartDrawZoomRectangle(int x, int y)
        {
            isZooming = true;
            zoomX1 = x;
            zoomY1 = y;
            zoomX2 = x;
            zoomY2 = y;

            DrawZoom();
        }

        public void UpdateZoomRectangle(int x, int y)
        {
            zoomX2 = x;
            zoomY2 = y;

            DrawZoom();
        }

        public void StopDrawZoomRectangle()
        {
            isZooming = false;
            SetVisibleGrid(
                Math.Min(zoomX1, zoomX2),
                Math.Min(zoomY1, zoomY2), 
                Math.Abs(zoomX1 - zoomX2),
                Math.Abs(zoomY1 - zoomY2));
        }

        public double TimeTick(double elapsedMs)
        {
            double lagMs = elapsedMs - lastMoveVirtualTimeMs;
            if (uiSettings.TrySkipTimeLag)
                notDrawTimer.Restart();

            if (elapsedMs - lastMoveVirtualTimeMs > uiSettings.MoveTimeMs)
            {
                isSuspended = false;
                if (lastIterationData != null)
                    world.ReleaseBattleAction(lastIterationData);
                lastIterationData = null;

                while (world.MovesCache.TryDequeue(out var data))
                {
                    if (data.WinSwarmIndex >= 0)
                    {
                        if (!isEnd)
                            MessageBox.Show($"SWARM {data.WinSwarmIndex} WIN");
                        isEnd = true;
                    }
                    if (lastIterationData != null)
                        world.ReleaseBattleAction(lastIterationData);
                    lastIterationData = data;
                    healthProgress = data.TotalSwarmHealth;
                    powerProgress = data.TotalSwarmPower;
                    manaProgress = data.TotalSwarmMana;

                    lastMoveVirtualTimeMs += uiSettings.MoveTimeMs; //important: aligned time

                    lagMs = elapsedMs - lastMoveVirtualTimeMs;

                    foreach (var entity in lastMoveDeadEntities)
                    {
                        if (uiSettings.DrawDeath && IsCellVisible(entity.Position))
                            deaths.Add(new DeathDraw(this, entity.Position, settings, lastMoveVirtualTimeMs));
                        entities.Remove(entity.Id);
                    }
                    lastMoveDeadEntities.Clear();
                    foreach (var entity in lastIterationData.DeadEntities)
                    {
                        lastMoveDeadEntities.Add(entity);
                    }

                    if (!uiSettings.TrySkipTimeLag)
                        break;
                    if (lagMs < 2*uiSettings.MoveTimeMs)
                        break;
                    if (notDrawTimer.ElapsedMilliseconds > 200) //min 5 draw per second)
                        break;
                }

                interactionPairs.Clear();

                if (lastIterationData == null)
                    //break; //no pre calculated moves
                    return lagMs; //no pre calculated moves

                bool flag = false;
                foreach (var move in lastIterationData.RealMoves)
                {
                    if (entities.TryGetValue(move.Entity.Id, out var drawEntity))
                        drawEntity.SetNextMove(move, lastMoveVirtualTimeMs); //important: aligned time
                    else
                        flag = true; //in case if manual add by click but not yet added to entities draw collection
                }

                if (!flag)
                {
                    foreach (var pair in lastIterationData.HealPairs)
                    {
                        if (entities[pair.Source.Id].IsVisible || entities[pair.Target.Id].IsVisible)
                            interactionPairs.Add(new ShootDraw(this, entities[pair.Source.Id], entities[pair.Target.Id], settings, Colors.Magenta, 1, false));
                        entities[pair.Target.Id].SetHealed();
                    }

                    foreach (var pair in lastIterationData.ShootPairs)
                    {
                        if (entities[pair.Source.Id].IsVisible || entities[pair.Target.Id].IsVisible)
                            interactionPairs.Add(new ShootDraw(this, entities[pair.Source.Id], entities[pair.Target.Id], settings, entities[pair.Source.Id].Color, 3, true));
                        entities[pair.Target.Id].SetShooted();
                    }

                    foreach (var pair in lastIterationData.InformationPairs)
                    {
                        if (entities[pair.Source.Id].IsVisible || entities[pair.Target.Id].IsVisible)
                            interactionPairs.Add(new ShootDraw(this, entities[pair.Source.Id], entities[pair.Target.Id], settings, Colors.YellowGreen, 1, false));
                        entities[pair.Target.Id].SetInformationReceived();
                    }
                }
            }

            var elapsedVirtualMs = Math.Min(elapsedMs, lastMoveVirtualTimeMs + uiSettings.MoveTimeMs);
            DrawBattleField(elapsedVirtualMs);
            DrawProgress();
            return lagMs;
        }

        private void DrawBattleField(double elapsedVirtualMs)
        {
            batlleBitmap.Clear();

            batlleBitmap.FillRectangle(0, progressHeightPx, ActualWidthPx, progressHeightPx + ActualHeightPx, backgroundColor);

            foreach (var kvp in entities)
            {
                kvp.Value.SetTime(elapsedVirtualMs);
            }
            foreach (var kvp in entities)
            {
                kvp.Value.DrawBackground(batlleBitmap);
            }

            foreach (var deathDraw in deaths)
            {
                if (deathDraw.Draw(batlleBitmap, elapsedVirtualMs))
                    deathDrawsTmp.Add(deathDraw);
            }

            foreach (var deathDraw in deathDrawsTmp)
            {
                deaths.Remove(deathDraw);
            }
            deathDrawsTmp.Clear();

            foreach (var line in interactionPairs)
            {
                line.Draw(batlleBitmap);
            }
            foreach (var kvp in entities)
            {
                kvp.Value.DrawForground(batlleBitmap);
            }

            DrawZoom();

            batlleBitmap.DrawRectangle(0, progressHeightPx, ActualWidthPx, progressHeightPx + ActualHeightPx, Colors.Black);
            batlleBitmap.FillRectangle(ActualWidthPx + 1, 0, bitmapWidthPx, progressHeightPx + bitmapHeightPx, Colors.White);
            batlleBitmap.FillRectangle(0, progressHeightPx + ActualHeightPx + 1, bitmapWidthPx, progressHeightPx + bitmapHeightPx, Colors.White);
        }

        private void DrawProgress()
        {
            //if (lastIterationData == null)
            //    return;
            for (int i = 0; i < swarmColors.Length; i++)
            {
                //DrawSwarmProgress(i, lastIterationData.TotalSwarmHealth[i], lastIterationData.TotalSwarmPower[i], lastIterationData.TotalSwarmMana[i], swarmColors[i]);
                DrawSwarmProgress(i, healthProgress[i], powerProgress[i], manaProgress[i], swarmColors[i]);
            }
        }

        private void DrawSwarmProgress(int swarmIndex, double totalHealth, double totalPower, double totalMana, Color swarmColor)
        {
            if (progressHeightPx == 0)
                return;
            int w = (int)((double)ActualWidthPx / swarmColors.Length);
            int xStart = swarmIndex * w;
            int h = progressHeightPx / 3;
            int healthWidth = Math.Max((int) (w * totalHealth), 2);
            int powerWidth = Math.Max((int)(w * totalPower), 2);
            int manaWidth = Math.Max((int)(w * totalMana), 2);
            batlleBitmap.FillRectangle(xStart, 0, xStart + w, progressHeightPx, swarmColor);
            batlleBitmap.FillRectangle(xStart + 1, 1, xStart + healthWidth - 1 , h - 1, Colors.Lime);
            batlleBitmap.FillRectangle(xStart + 1, h + 1, xStart + powerWidth - 1 , 2*h - 1, Colors.Yellow);
            batlleBitmap.FillRectangle(xStart + 1, 2*h + 1, xStart + manaWidth - 1 , 3*h - 1, Colors.Magenta);
        }

        private void DrawZoom()
        {
            if (!isZooming) 
                return;
            int x1 = Math.Min(zoomX1, zoomX2);
            int x2 = Math.Max(zoomX1, zoomX2);
            int y1 = Math.Min(zoomY1, zoomY2);
            int y2 = Math.Max(zoomY1, zoomY2);
            batlleBitmap.DrawRectangle(x1, y1, x2, y2, Colors.Black);
        }

        public void ConvertCoordinates(Cell cell, out double xPx, out double yPx)
        {
            xPx = (cell.X - battleStartX) * settings.UiSettings.PixelsPerCell;
            yPx = progressHeightPx + (battleVisibleHeight - (cell.Y - battleStartY) - 1) * settings.UiSettings.PixelsPerCell;
        }

        public bool IsSuspended()
        {
            return isSuspended;
        }

        public bool IsCellVisible(Cell cell)
        {
            return cell.X >= battleStartX && cell.X < battleStartX + battleVisibleWidth 
                   && cell.Y >= battleStartY && cell.Y < battleStartY + battleVisibleHeight;
        }

        private void SetVisibleGrid(int xLeftPx, int yTopPx, int widthPx, int heightPx)
        {
            double x1Px = xLeftPx;
            //double y1Px = bitmapHeightPx - yTopPx - heightPx + progressHeightPx;
            double y1Px = ActualHeightPx - yTopPx - heightPx + progressHeightPx;
            int xCellDelta = (int)(x1Px / uiSettings.PixelsPerCell);
            int yCellDelta = (int)(y1Px / uiSettings.PixelsPerCell);

            //int newVisibleWidth = (int)(battleVisibleWidth * widthPx / (double)bitmapWidthPx);
            //int newVisibleHeight = (int)(battleVisibleHeight * heightPx / (double)bitmapHeightPx);
            int newVisibleWidth = (int)(widthPx / uiSettings.PixelsPerCell);
            int newVisibleHeight = (int)(heightPx / uiSettings.PixelsPerCell);

            SetGridInner(battleStartX + xCellDelta, battleStartY + yCellDelta, newVisibleWidth, newVisibleHeight, true);
        }

        public void ResetGrid(bool suspend = true)
        {
            SetGridInner(0, 0, settings.BattleSettings.Width, settings.BattleSettings.Height, suspend);
        }

        private int ActualWidthPx => (int) (battleVisibleWidth * uiSettings.PixelsPerCell);
        private int ActualHeightPx => (int) (battleVisibleHeight * uiSettings.PixelsPerCell);


        private void SetGridInner(int xStart, int yStart, int width, int height, bool suspend)
        {
            isSuspended = suspend;

            battleStartX = xStart;
            battleStartY = yStart;
            battleVisibleWidth = width;
            battleVisibleHeight = height;

            CalcPixelsPerCell();
        }

        private void CalcPixelsPerCell()
        {
            var perX = bitmapWidthPx / (double)Math.Max(battleVisibleWidth, 1);
            var perY = bitmapHeightPx / (double)Math.Max(battleVisibleHeight, 1);
            double min = Math.Min(perX, perY);
            uiSettings.PixelsPerCell = min;
        }

        public void AddEntityToGrid(int swarmId, int xPx, int yPx)
        {
            //yPx = bitmapHeightPx - yPx + progressHeightPx;
            yPx = ActualHeightPx - yPx + progressHeightPx;
            int xCell = battleStartX + (int)(xPx / uiSettings.PixelsPerCell);
            int yCell = battleStartY + (int)(yPx / uiSettings.PixelsPerCell);
            world.AddEntity(swarmId, xCell, yCell);
        }

        public void SetSpeed()
        {
            if (uiSettings.MoveTimeMs >= 1 && uiSettings.MoveTimeMs <= 100)
                world.SetPreCalcCapacity((int)(100.0/Math.Log(uiSettings.MoveTimeMs + 1))); 
            else
                world.SetPreCalcCapacity(0);
        }
    }
}