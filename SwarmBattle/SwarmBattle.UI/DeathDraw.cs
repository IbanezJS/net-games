﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using SwarmBattle.Core;

namespace SwarmBattle.UI
{
    public class DeathDraw
    {
        private class Item
        {
            public int X;
            public int Y;
            public double StartRadius;
            public double MaxDeltaRadius;
        }

        private readonly ICoordinateConverter converter;
        private readonly double timeSnapshotMs;
        private byte currentFadeAlpha;
        private readonly Item[] circles;
        private readonly double totalTimeMs;
        
        public DeathDraw(ICoordinateConverter converter, Cell entityCell, Settings settings, double timeSnapshotMs)
        {
            var rnd = new Random(Guid.NewGuid().GetHashCode());
            this.converter = converter;
            this.timeSnapshotMs = timeSnapshotMs;
            int count = rnd.Next(4, 10); 
            circles = new Item[count];
            var perCell = settings.UiSettings.PixelsPerCell;
            totalTimeMs = Math.Max(settings.UiSettings.MoveTimeMs * settings.UiSettings.DeathMoveCount, settings.UiSettings.MoveTimeMs);
            converter.ConvertCoordinates(entityCell, out var xPx, out var yPx);
            xPx += perCell * 0.5;
            yPx += perCell * 0.5;
            for (int i = 0; i < count; i++)
            {
                int x = (int) (xPx + (rnd.NextDouble() - 0.5) * perCell);
                int y = (int) (yPx + (rnd.NextDouble() - 0.5) * perCell);
                double radius = rnd.NextDouble() * perCell* 0.5;
                double maxDeltaRadius = rnd.NextDouble() * perCell * 0.5;
                circles[i] = new Item {X = x, Y = y, StartRadius = radius, MaxDeltaRadius = maxDeltaRadius};
            }
        }
       
        public bool Draw(WriteableBitmap bitmap, double elapsedMs)
        {
            if (converter.IsSuspended())
                return false;

            double deltaMs = Math.Min(elapsedMs - timeSnapshotMs, totalTimeMs); 
            double timeFactor = deltaMs / totalTimeMs;
            currentFadeAlpha = (byte)(255 * (1 - timeFactor));
            foreach (var item in circles)
            {
                int radius = (int)(item.StartRadius + item.MaxDeltaRadius * timeFactor);
                bitmap.FillEllipseCentered(item.X, item.Y, radius, radius, Colors.DarkRed.Alpha(currentFadeAlpha));
            }
            return elapsedMs - timeSnapshotMs >= totalTimeMs;
        }
    }
}