﻿using SwarmBattle.Core;

namespace SwarmBattle.UI
{
    public class Settings
    {
        public UISettings UiSettings { get; set; }
        public BattleSettings BattleSettings { get; set; }
    }
}
