﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Newtonsoft.Json;
using SwarmBattle.Core;

namespace SwarmBattle.UI
{
    public partial class BattleFieldControl : UserControl
    {
        private readonly BattleSettings battleSettings;
        private readonly UISettings uiSettings;

        private BattleWorld world;
        private WriteableBitmap battleBitmapWriter;
        private DrawManager drawManager;
        private Stopwatch timer;
        private int widthPx;
        private int heightPx;

        private readonly Dispatcher uiDispatcher;
        
        private double resizeFactorX = 1;
        private double resizeFactorY = 1;

        public BattleFieldControl()
        {
            var json = File.ReadAllText("Settings/BattleSettings.json", Encoding.UTF8);
            battleSettings = JsonConvert.DeserializeObject<BattleSettings>(json);
            json = File.ReadAllText("Settings/UIsettings.json", Encoding.UTF8);
            uiSettings = JsonConvert.DeserializeObject<UISettings>(json);

            InitializeComponent();
            
            Strategies = new ObservableCollection<string>(battleSettings.SwormStrategies);

            DataContext = this;
            
            SpeedSlider.Minimum = Math.Log(1); 
            SpeedSlider.Maximum = Math.Log(1000);
            SpeedSlider.Value = Math.Log(uiSettings.MoveTimeMs);
            SpeedSlider.IsDirectionReversed = true;

            SetSpeed();

            StrategyTypeSelect.ItemsSource = Core.Helper.GetStrategyTypes().Select(kvp => kvp.Key).ToList();
            StrategyTypeSelect.SelectedIndex = 0;

            uiDispatcher = Dispatcher;
        }
        
        private void MainImage_OnLoaded(object sender, RoutedEventArgs e)
        {
            widthPx = (int)MainGrid.ColumnDefinitions[1].ActualWidth;
            heightPx = (int)MainGrid.ActualHeight;

            battleBitmapWriter = BitmapFactory.New(widthPx, heightPx);
            MainImage.Source = battleBitmapWriter;

            CompositionTarget.Rendering += CompositionTargetRendering;
        }

        private void CompositionTargetRendering(object sender, EventArgs e)
        {
            Draw();
        }
        
        private void Draw()
        {
            if (drawManager == null)
                return;
            var lagMs = drawManager.TimeTick(timer.Elapsed.TotalMilliseconds);
            TextBlockLag.Text = lagMs > 0 ? $"Lag = {lagMs / 1000:0.00}s" : $"Lag = 0s";
        }

        public bool DrawVision
        {
            get => uiSettings.DrawVision;
            set => uiSettings.DrawVision = value;
        }

        public bool DrawBlood
        {
            get => uiSettings.DrawBlood;
            set => uiSettings.DrawBlood = value;
        }
        public bool DrawDeath
        {
            get => uiSettings.DrawDeath;
            set => uiSettings.DrawDeath = value;
        }

        public bool DrawProgressBar
        {
            get => uiSettings.DrawProgressBar;
            set
            {
                uiSettings.DrawProgressBar = value;
                drawManager?.UpdateDrawProgressSettings();
            }
        }

        public bool AllowFire
        {
            get => battleSettings.AllowFire;
            set => battleSettings.AllowFire = value;
        }

        public bool AllowHeal
        {
            get => battleSettings.AllowHeal;
            set => battleSettings.AllowHeal = value;
        }

        public bool InfinitePower
        {
            get => battleSettings.InfinitePower;
            set => battleSettings.InfinitePower = value;
        }

        public bool InfiniteMana
        {
            get => battleSettings.InfiniteMana;
            set => battleSettings.InfiniteMana = value;
        }

        public bool AddByClick
        {
            get => uiSettings.AddByClick;
            set => uiSettings.AddByClick = value;
        }

        public bool TrySkipLag
        {
            get => uiSettings.TrySkipTimeLag;
            set => uiSettings.TrySkipTimeLag = value;
        }

        public ObservableCollection<string> Strategies { get; }

        private void ButtonAddSwarmOnClick(object sender, RoutedEventArgs e)
        {
            Strategies.Add(StrategyTypeSelect.SelectedValue.ToString());
        }

        private void ButtonDeleteSwarmOnClick(object sender, RoutedEventArgs e)
        {
            if (StrategyList.SelectedIndex < 0)
                return;
            Strategies.Remove(StrategyList.SelectedValue.ToString());
        }

        private void SpeedSliderOnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!IsLoaded)
                return;
            SetSpeed();
        }

        private void SetSpeed()
        {
            uiSettings.MoveTimeMs = Math.Pow(Math.E, SpeedSlider.Value);
            TextBlockSpeed.Text = $"Speed = {(int)uiSettings.MoveTimeMs} ms/move";
            drawManager?.SetSpeed();
        }

        private void ButtonStartOnClick(object sender, RoutedEventArgs e)
        {
            battleSettings.SwormStrategies = Strategies.ToList();

            if (battleSettings.ThreadCount <= 0 || battleSettings.ThreadCount >= Environment.ProcessorCount)
                battleSettings.ThreadCount = Math.Max(1, Environment.ProcessorCount - 1); //1 thread for UI

            world?.StopSimulation();

            world = new BattleWorld(battleSettings);

            drawManager = new DrawManager(
                world, 
                new Settings
                {
                    BattleSettings = battleSettings,
                    UiSettings = uiSettings
                },
                battleBitmapWriter,
                widthPx,
                heightPx, 
                Colors.FloralWhite, //todo background color to settings
                uiDispatcher);

            SetSpeed();

            world.StartSimulation();
            
            timer = Stopwatch.StartNew();
        }
        
        private void MainImage_OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (drawManager == null)
                return;
            var pos = Mouse.GetPosition(MainImage);
            drawManager.StartDrawZoomRectangle((int)(pos.X * resizeFactorX), (int)(pos.Y * resizeFactorY));
        }

        private void MainImage_OnMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            drawManager?.StopDrawZoomRectangle();
        }

        private void MainImage_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (drawManager == null)
                return;
            if (e.RightButton == MouseButtonState.Released)
                return;
            var pos = Mouse.GetPosition(MainImage);
            drawManager.UpdateZoomRectangle((int)(pos.X * resizeFactorX), (int)(pos.Y * resizeFactorY));
        }

        private void MainImage_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount >= 2)
                drawManager?.ResetGrid();
            else if (uiSettings.AddByClick && StrategyList.Items.Count > 0)
            {
                if (StrategyList.Items.Count > 1 && StrategyList.SelectedIndex < 0)
                    MessageBox.Show("Select swarm from list");
                else
                {
                    int swarmId = StrategyList.SelectedIndex >= 0 ? StrategyList.SelectedIndex : 0;
                    var pos = Mouse.GetPosition(MainImage);
                    drawManager?.AddEntityToGrid(swarmId, (int)(pos.X * resizeFactorX), (int)(pos.Y * resizeFactorY));
                }
            }
        }

        private void MainImage_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!IsLoaded)
                return;
            
            double newWidthPx = MainImage.ActualWidth;
            double newHeightPx = MainImage.ActualHeight;
            if (newWidthPx > 0 && newHeightPx > 0)
            {
                resizeFactorX = widthPx / newWidthPx;
                resizeFactorY = heightPx / newHeightPx;
            }
            else
            {
                resizeFactorX = 1;
                resizeFactorY = 1;
            }
        }
    }
}
