﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace SolarSystem
{
    public struct Wave
    {
        public double Mass;
        public double X;
        public double Y;
        public double Z;
        public int TimeStart;
        public int VersionTime;
    }

    public class Planet
    {
        public int Index;
        public bool IsFinalized = false;
        public double Mass;
        public double Radius;
        public double X;
        public double Y;
        public double Z;
        public double Xtarget;
        public double Ytarget;
        public double Ztarget;
        public double Vx;
        public double Vy;
        public double Vz;
        public bool DarkMatter;
        public FastQueueFixedSize<Wave> Waves;
        public List<int> WaveByPlanetIndex;
        public Wave SelectedWave;
        public int SelectedWaveVersion;
    }

    public struct WaveSnap
    {
        public double X;
        public double Y;
        public double Z;
        public double RadiusFrom;
        public double RadiusTo;
    }

    public struct PlanetSnap
    {
        public double Radius;
        public double Xfrom;
        public double Yfrom;
        public double Zfrom;
        public double Xto;
        public double Yto;
        public double Zto;
    }

    public class MoveInfo
    {
        public List<PlanetSnap> Planets;
        public List<WaveSnap> Waves;
        public PlanetSnap BiggestPlanet;

        public MoveInfo(int capacity)
        {
            Planets = new List<PlanetSnap>(capacity);
            Waves = new List<WaveSnap>(capacity);
        }

        public void Clear()
        {
            Planets.Clear();
            Waves.Clear();
            BiggestPlanet = default;
        }

        public void Merge(MoveInfo other)
        {
            Planets.AddRange(other.Planets);
            Waves.AddRange(other.Waves);
        }
    }

    public class SolarSimulation
    {
        private class ThreadContext
        {
            public readonly int ThreadIndex;
            public readonly MoveInfo LocalMoveInfo;
            public readonly ManualResetEvent StartIterationWaitEvent;
            public readonly ManualResetEvent EndIterationWaitEvent;

            public ThreadContext(
                int threadIndex,
                MoveInfo localMoveInfo,
                ManualResetEvent startIterationWaitEvent,
                ManualResetEvent endIterationWaitEvent)
            {
                ThreadIndex = threadIndex;
                LocalMoveInfo = localMoveInfo;
                StartIterationWaitEvent = startIterationWaitEvent;
                EndIterationWaitEvent = endIterationWaitEvent;
            }
        }

        private readonly Settings settings;
        private readonly List<Planet> allPlanets; //keep only merged
        private readonly List<Planet> toAddPlanets;
        private readonly int planetsCapacity;
        private int planetSequence = 0;
        private volatile bool wasCollisions;
        private volatile bool running;
        private long exitCounter;
        private Thread thread;
        private Thread[] threads;
        private ThreadContext[] threadContexts;
        private ManualResetEvent[] threadsWorkEndWait;
        private readonly ConcurrentQueue<MoveInfo> movePool = new ConcurrentQueue<MoveInfo>();
        private volatile int maxCalcMoveCacheCount = 10;
        private int currentTime = 0;
        
        public readonly ConcurrentQueue<MoveInfo> MovesCache = new ConcurrentQueue<MoveInfo>();
        
        public SolarSimulation(Settings settings)
        {
            this.settings = settings;
            toAddPlanets = new List<Planet>();
            
            planetsCapacity = (settings.StarPlanets.Count
                             + settings.GalaxyList.Sum(g => g.PlanetCount)) * 2;

            allPlanets = new List<Planet>(planetsCapacity);

            var rnd = new Random(Guid.NewGuid().GetHashCode());

            foreach (var gSettings in settings.GalaxyList)
            {
                for (int i = 0; i < gSettings.PlanetCount; i++)
                {
                    double x = gSettings.XCenter;
                    double y = gSettings.YCenter;

                    double xSpan = gSettings.XBox / 3.0;
                    double ySpan = gSettings.YBox / 3.0;

                    while ((Math.Abs(x - gSettings.XCenter) < xSpan * 1.0)
                        && (Math.Abs(y - gSettings.YCenter) < ySpan * 1.0))
                    {
                        double shiftSignX = (rnd.NextDouble() < 0.5) ? -1 : 1;
                        double shiftSignY = (rnd.NextDouble() < 0.5) ? -1 : 1;

                        double shiftX = (rnd.NextDouble() < 0.45) ? 0 : (rnd.NextDouble() < 0.9) ? 1 : 2;
                        double shiftY = (rnd.NextDouble() < 0.45) ? 0 : (rnd.NextDouble() < 0.9) ? 1 : 2;

                        x = gSettings.XCenter + shiftSignX * xSpan * (shiftX + rnd.NextDouble());
                        y = gSettings.YCenter + shiftSignY * ySpan * (shiftY + rnd.NextDouble());
                    }

                    double z = gSettings.ZCenter + (rnd.NextDouble() - 0.5) * gSettings.ZBox;

                    double vx = 0;
                    double vy = 0;
                    double vz = 0;

                    switch (gSettings.InitVelocityKind)
                    {
                        case InitVelocityKind.None:
                            break;
                        case InitVelocityKind.Random:
                            vx = (rnd.NextDouble() - 0.5) * 2 * gSettings.InitVelocity;
                            vy = (rnd.NextDouble() - 0.5) * 2 * gSettings.InitVelocity;
                            vz = (rnd.NextDouble() - 0.5) * 2 * gSettings.InitVelocity;
                            break;
                        case InitVelocityKind.Tangent:
                            double phi = Math.Atan2(y - gSettings.YCenter, x - gSettings.XCenter);
                            vx = -gSettings.InitVelocity * Math.Sin(phi);
                            vy = gSettings.InitVelocity * Math.Cos(phi);
                            vz = 0;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    var planet = new Planet
                    {
                        Mass = gSettings.InitMass,
                        Radius = settings.CalcRadiusByMass(gSettings.InitMass),
                        X = x,
                        Y = y,
                        Z = z,
                        Xtarget = x,
                        Ytarget = y,
                        Ztarget = z,
                        Vx = vx,
                        Vy = vy,
                        Vz = vz,
                        DarkMatter = gSettings.DarkMatter
                    };

                    AddPlanet(planet);
                }
            }

            foreach (var set in settings.StarPlanets)
            {
                var planet = new Planet
                {
                    Mass = set.InitMass,
                    Radius = settings.CalcRadiusByMass(set.InitMass),
                    X = set.X,
                    Y = set.Y,
                    Z = set.Z,
                    Xtarget = set.X,
                    Ytarget = set.Y,
                    Ztarget = set.Z,
                    Vx = set.VelocityX,
                    Vy = set.VelocityY,
                    Vz = set.VelocityZ,
                    DarkMatter = set.DarkMatter
                };
                AddPlanet(planet);
            }
        }
        
        public void AddPlanet(Planet planet)
        {
            //TODO support UseGravityWaves change on-fly ?
            planet.Index = planetSequence++;
            if (settings.UseGravityWaves)
            {
                planet.Waves = new FastQueueFixedSize<Wave>(settings.MaxWavesCount);
                planet.WaveByPlanetIndex = new List<int>(planetsCapacity);
                for (int i = 0; i < planet.Index; i++)
                {
                    planet.WaveByPlanetIndex.Add(-1);
                }
            }

            allPlanets.Add(planet);

            if (settings.UseGravityWaves)
            {
                foreach (var p in allPlanets)
                {
                    p.WaveByPlanetIndex.Add(-1);
                }
                Debug.Assert(allPlanets.All(p => p.WaveByPlanetIndex.Count == planetSequence));
            }
        }

        public int PlanetCount => allPlanets.Count;

        public void StartSimulation()
        {
            running = true;
            currentTime = 0;

            if (settings.ThreadCount <= 0)
                settings.ThreadCount = Environment.ProcessorCount;
            threads = new Thread[settings.ThreadCount];
            threadContexts = new ThreadContext[settings.ThreadCount];
            threadsWorkEndWait = new ManualResetEvent[settings.ThreadCount];
            int allPlanetCount = (int)((double)allPlanets.Count / settings.ThreadCount) + 1;
            exitCounter = 1 + settings.ThreadCount;
            for (int i = 0; i < settings.ThreadCount; i++)
            {
                var startIterationWait = new ManualResetEvent(false);
                var endIterationWait = new ManualResetEvent(false);
                threadsWorkEndWait[i] = endIterationWait;
                threadContexts[i] = new ThreadContext(i,
                    new MoveInfo(allPlanetCount), 
                    startIterationWait,
                    endIterationWait);
                threads[i] = new Thread(OneMoveCalcLoop) { IsBackground = true };
                threads[i].Start(threadContexts[i]);
            }

            thread = new Thread(MainCalcLoop) { IsBackground = true };
            thread.Start();
        }

        public void StopSimulation()
        {
            running = false;
            while (Interlocked.Read(ref exitCounter) > 0)
            {
                Thread.Sleep(1);
            }
        }

        private void MainCalcLoop()
        {
            while (running)
            {
                if (MovesCache.Count >= maxCalcMoveCacheCount)
                {
                    Thread.Sleep(0);
                    continue;
                }

                foreach (var ctx in threadContexts)
                {
                    ctx.StartIterationWaitEvent.Set();
                }

                //wait for all threads to complete
                WaitHandle.WaitAll(threadsWorkEndWait);
                foreach (var handle in threadsWorkEndWait)
                {
                    handle.Reset();
                }

                if (!movePool.TryDequeue(out var result))
                    result = new MoveInfo(allPlanets.Count);

                result.Clear();
                foreach (var ctx in threadContexts)
                {
                    result.Merge(ctx.LocalMoveInfo);
                }

                CommitMove(result);

                ++currentTime;

                MovesCache.Enqueue(result);
            }

            foreach (var ctx in threadContexts)
            {
                ctx.StartIterationWaitEvent.Set();
            }

            Interlocked.Decrement(ref exitCounter);
        }

        public void ReleaseMove(MoveInfo info)
        {
            movePool.Enqueue(info);
        }

        private void OneMoveCalcLoop(object data)
        {
            var ctx = (ThreadContext)data;
            while (running)
            {
                ctx.StartIterationWaitEvent.WaitOne();
                ctx.StartIterationWaitEvent.Reset();

                int allCount = allPlanets.Count;
                int perThread = (int)((double)allCount / settings.ThreadCount) + 1;

                int startIndex = ctx.ThreadIndex * perThread;
                int count = Math.Max(0, Math.Min(allCount - startIndex, perThread));

                DoMove(ctx, startIndex, count);

                ctx.EndIterationWaitEvent.Set();
            }
            Interlocked.Decrement(ref exitCounter);
        }

        private double CalcWaveRadius(ref Wave wave, Planet planet
            , ref double dx, ref double dy, ref double dz, ref double r)
        {
            double waveRadius = (currentTime - wave.TimeStart) * settings.GravityWavesSpeed;
            dx = wave.X - planet.X;
            dy = wave.Y - planet.Y;
            dz = wave.Z - planet.Z;
            r = Math.Sqrt(dx * dx + dy * dy + dz * dz);
            return waveRadius;
        }

        private void DoMove(ThreadContext ctx, int startIndex, int count)
        {
            ctx.LocalMoveInfo.Clear();
            if (count <= 0)
                return;
            for (var i = 0; i < count; i++)
            {
                var planet = allPlanets[startIndex + i];
                double m = planet.Mass;
                double dt = settings.DeltaT;
                double factor = settings.GravityFactor;
                double sumFx = 0;
                double sumFy = 0;
                double sumFz = 0;
                bool isCollision = false;
                foreach (var other in allPlanets)
                {
                    if (ReferenceEquals(other, planet))
                    {
                        Debug.Assert(planet.Index == other.Index);
                        if (settings.UseGravityWaves && (allPlanets.Count == 1) && (planet.Waves.Count > 0))
                        {
                            planet.Waves.PeekByRef().VersionTime = currentTime;
                        }
                        continue;
                    }

                    double m2 = 0;
                    double dx = 0, dy = 0, dz = 0;
                    double r = 0;
                    if (settings.UseGravityWaves)
                    {
                        if (other.Waves.Count == 0)
                            continue;

                        int waveIndex = planet.WaveByPlanetIndex[other.Index];
                        waveIndex += currentTime;
                        if ((waveIndex < 0) || (waveIndex >= other.Waves.Count))
                        {
                            waveIndex = other.Waves.Count - 1;
                        }
                        // note: waveIndex from tail
                        int headIndex = other.Waves.Count - 1 - waveIndex;
                        Debug.Assert((headIndex >= 0) && (headIndex < other.Waves.Count));
                        bool intersection = false;
                        while (headIndex < other.Waves.Count)
                        {
                            ref Wave wave = ref other.Waves[headIndex];
                            // TODO check prev index
                            double btwRadius = 0;
                            double waveRadius = CalcWaveRadius(ref wave, planet
                                , ref dx, ref dy, ref dz, ref btwRadius);
                            if (waveRadius > btwRadius)
                            {
                                m2 = wave.Mass; // IMPORTANT: to use wave snapshot mass (not planet Mass)
                                r = btwRadius;
                                intersection = true;
                                ++headIndex;
                            }
                            else
                            {
                                wave.VersionTime = currentTime;
                                break;
                            }
                        }
                        if (intersection)
                        {
                            --headIndex;
                            Debug.Assert(headIndex >= 0);
                            // note: waveIndex from tail
                            waveIndex = other.Waves.Count - 1 - headIndex;
                            planet.WaveByPlanetIndex[other.Index] = waveIndex;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        m2 = other.Mass;
                        dx = other.X - planet.X;
                        dy = other.Y - planet.Y;
                        dz = other.Z - planet.Z;
                        r = Math.Sqrt(dx * dx + dy * dy + dz * dz);
                    }

                    if ((r < planet.Radius + other.Radius) && !planet.DarkMatter && !other.DarkMatter)
                    {
                        //collision here
                        isCollision = true;
                        lock (toAddPlanets)
                        {
                            if (!planet.IsFinalized && !other.IsFinalized)
                            {
                                var newPlanet = MakeCollision(planet, other);
                                toAddPlanets.Add(newPlanet);
                                wasCollisions = true;
                                ctx.LocalMoveInfo.Planets.Add(new PlanetSnap
                                {
                                    Radius = newPlanet.Radius,
                                    Xfrom = newPlanet.X,
                                    Yfrom = newPlanet.Y,
                                    Zfrom = newPlanet.Z,
                                    Xto = newPlanet.Xtarget,
                                    Yto = newPlanet.Ytarget,
                                    Zto = newPlanet.Ztarget
                                });
                            }
                        }
                        break;
                    }

                    Debug.Assert(m > 0);
                    Debug.Assert(m2 > 0);
                    Debug.Assert(r > 0);
                    var gravityFactor = factor * m * m2 / (r * r);
                    //var gravityFactor = factor * m * other.Mass * Math.Sin(r) / (r * r * r);
                    var fx = dx * gravityFactor; // dx/r = cos(fx)
                    var fy = dy * gravityFactor; // dy/r = cos(fy)
                    var fz = dz * gravityFactor; // dz/r = cos(fz)
                    sumFx += fx;
                    sumFy += fy;
                    sumFz += fz;
                }
                if (isCollision)
                    continue;
                //F=ma
                //dv = a*dt;
                //ds = v*dt;
                var ax = sumFx / m;
                var ay = sumFy / m;
                var az = sumFz / m;
                planet.Vx += ax * dt;
                planet.Vy += ay * dt;
                planet.Vz += az * dt;
                planet.Xtarget = planet.X + planet.Vx * dt;
                planet.Ytarget = planet.Y + planet.Vy * dt;
                planet.Ztarget = planet.Z + planet.Vz * dt;
                
                ctx.LocalMoveInfo.Planets.Add(new PlanetSnap
                {
                    Radius = planet.Radius,
                    Xfrom = planet.X,
                    Yfrom = planet.Y,
                    Zfrom = planet.Z,
                    Xto = planet.Xtarget,
                    Yto = planet.Ytarget,
                    Zto = planet.Ztarget
                });

                if (settings.DrawGravityWaves)
                {
                    double waveRadiusFrom = (currentTime - planet.SelectedWave.TimeStart) * settings.GravityWavesSpeed;
                    double waveRadiusTo = waveRadiusFrom + settings.GravityWavesSpeed;
                    ctx.LocalMoveInfo.Waves.Add(new WaveSnap
                    {
                        X = planet.SelectedWave.X,
                        Y = planet.SelectedWave.Y,
                        Z = planet.SelectedWave.Z,
                        RadiusFrom = waveRadiusFrom,
                        RadiusTo = waveRadiusTo
                    });
                }
            }
        }

        private Planet MakeCollision(Planet p1, Planet p2)
        {
            Debug.Assert(!p1.DarkMatter && !p2.DarkMatter);
            p1.IsFinalized = true;
            p2.IsFinalized = true;
            var maxPlanet = p1.Mass >= p2.Mass ? p1 : p2;
            var m1 = p1.Mass;
            var m2 = p2.Mass;
            var f1 = m1 / (m1 + m2);
            var f2 = m2 / (m1 + m2);
            var vx1 = f1 * p1.Vx;
            var vy1 = f1 * p1.Vy;
            var vz1 = f1 * p1.Vz;
            var vx2 = f2 * p2.Vx;
            var vy2 = f2 * p2.Vy;
            var vz2 = f2 * p2.Vz;
            var x = f1 * p1.X + f2 * p2.X;
            var y = f1 * p1.Y + f2 * p2.Y;
            var z = f1 * p1.Z + f2 * p2.Z;
            var xTarget = f1 * p1.Xtarget + f2 * p2.Xtarget;
            var yTarget = f1 * p1.Ytarget + f2 * p2.Ytarget;
            var zTarget = f1 * p1.Ztarget + f2 * p2.Ztarget;
            var r1 = p1.Radius;
            var r2 = p2.Radius;
            var newR = Math.Pow(r1 * r1 * r1 + r2 * r2 * r2, 1.0/3.0);
            return new Planet
            {
                Mass = m1 + m2,
                Radius = newR,
                X = x,
                Y = y,
                Z = z,
                Xtarget = xTarget,
                Ytarget = yTarget,
                Ztarget = zTarget,
                Vx = vx1 + vx2,
                Vy = vy1 + vy2,
                Vz = vz1 + vz2,
                DarkMatter = false
            };
        }

        private void CommitMove(MoveInfo result)
        {
            if (wasCollisions)
            {
                allPlanets.RemoveAll(p => p.IsFinalized);
                wasCollisions = false;
            }
            lock (toAddPlanets)
            {
                foreach (var planet in toAddPlanets)
                {
                    AddPlanet(planet);
                }
                toAddPlanets.Clear();
            }

            var max = allPlanets[0];
            foreach (var planet in allPlanets)
            {
                planet.X = planet.Xtarget;
                planet.Y = planet.Ytarget;
                planet.Z = planet.Ztarget;
                double r = Math.Sqrt(planet.X * planet.X + planet.Y * planet.Y + planet.Z * planet.Z);
                if (r > settings.MaxUniverseSize)
                {
                    // TODO half of all avg speed
                    planet.Vx = -Math.Sign(planet.Vx);
                    planet.Vy = -Math.Sign(planet.Vy);
                    planet.Vz = -Math.Sign(planet.Vz);
                    planet.X += -Math.Sign(planet.X) * planet.Radius;
                    planet.Y += -Math.Sign(planet.Y) * planet.Radius;
                    planet.Z += -Math.Sign(planet.Z) * planet.Radius;
                    if (Math.Abs(planet.X) > settings.MaxUniverseSize)
                        planet.X = Math.Sign(planet.X) * settings.MaxUniverseSize;
                    if (Math.Abs(planet.Y) > settings.MaxUniverseSize)
                        planet.Y = Math.Sign(planet.Y) * settings.MaxUniverseSize;
                    if (Math.Abs(planet.Z) > settings.MaxUniverseSize)
                        planet.Z = Math.Sign(planet.Z) * settings.MaxUniverseSize;
                }
                if (planet.Mass > max.Mass)
                    max = planet;
                if (!settings.UseGravityWaves)
                    continue;

                bool selectNewWave = (planet.Waves.Count == 0);
                while (planet.Waves.Count > 1)
                {
                    ref Wave wave = ref planet.Waves.PeekByRef();
                    if ((wave.VersionTime == currentTime) && !planet.Waves.Full)
                        break;
                    if (wave.TimeStart == planet.SelectedWaveVersion)
                        selectNewWave = true;
                    planet.Waves.DequeueBatchSilently(1);
                    planet.Waves.PeekByRef().VersionTime = currentTime;
                }

                Debug.Assert(!planet.Waves.Full 
                          && ((planet.Waves.Count <= 1)
                           || (planet.Waves.PeekByRef().VersionTime == currentTime)));

                planet.Waves.Enqueue(new Wave
                {
                    Mass = planet.Mass,
                    TimeStart = currentTime,
                    VersionTime = 0,
                    X = planet.X,
                    Y = planet.Y,
                    Z = planet.Z,
                });
                if (selectNewWave)
                {
                    planet.SelectedWaveVersion = currentTime;
                    planet.SelectedWave = planet.Waves[planet.Waves.Count - 1];
                }
            }
            result.BiggestPlanet = new PlanetSnap
            {
                Radius = max.Radius,
                Xto = max.X,
                Yto = max.Y,
                Zto = max.Z
                //from no matter
            };
        }
        
        public void AddPlanetDynamic(double x, double y, double z)
        {
            lock (toAddPlanets)
            {
                if ((settings.GalaxyList.Count == 0) && (settings.StarPlanets.Count == 0))
                    return;
                double initM = 0;
                double initR = 0;
                double initV = 0;
                if (settings.GalaxyList.Count > 0)
                {
                    var set = settings.GalaxyList[0];
                    initM = set.InitMass;
                    initR = settings.CalcRadiusByMass(initM);
                    initV = set.InitVelocity;
                }
                else
                {
                    var set = settings.StarPlanets[0];
                    initM = set.InitMass;
                    initR = settings.CalcRadiusByMass(initM);
                    initV = 0;
                }
                var planet = new Planet
                {
                    Mass = initM,
                    Radius = initR,
                    X = x,
                    Y = y,
                    Z = z,
                    Xtarget = x,
                    Ytarget = y,
                    Ztarget = z,
                    DarkMatter = settings.ClickAddDarkMatter
                };
                var rnd = new Random(Guid.NewGuid().GetHashCode());
                planet.Vx = (rnd.NextDouble() - 0.5) * 2 * initV;
                planet.Vy = (rnd.NextDouble() - 0.5) * 2 * initV;
                planet.Vz = (rnd.NextDouble() - 0.5) * 2 * initV;

                toAddPlanets.Add(planet);
            }
        }

        public void SetPreCalcCapacity(int capacity)
        {
            maxCalcMoveCacheCount = Math.Max(15, capacity);
        }
    }
}
