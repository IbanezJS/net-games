﻿using System;
using System.Collections.Generic;

namespace SolarSystem
{
    public class Settings
    {
        public int MaxStarsCount { get; set; } = 200;
        public int ThreadCount { get; set; } = -1;
        public double MoveTimeMs { get; set; } = 100;
        public bool TrySkipTimeLag { get; set; } = true;
        public int CameraRadius { get; set; } = 500;
        public int CameraDelta { get; set; } = 50;
        public double CameraTheta { get; set; } = 45;
        public double CameraPhi { get; set; } = 45;
        public bool DrawAxes { get; set; } = true;
        public bool UseSimpleDraw { get; set; } = false;
        public double GravityFactor { get; set; } = 1;
        public double DeltaT { get; set; } = 1;
        public bool UseGravityWaves { get; set; } = true;
        public double GravityWavesSpeed { get; set; } = 50;
        public int MaxWavesCount { get; set; } = 2048;
        public bool DrawGravityWaves { get; set; } = false;
        public bool ClickAddDarkMatter { get; set; } = false;
        public double MaxUniverseSize { get; set; } = 5000000;
        public double RadiusForOneMass { get; set; } = 10;
        public List<Galaxy> GalaxyList { get; set; } = new List<Galaxy>();
        public List<StarPlanet> StarPlanets { get; set; } = new List<StarPlanet>();

        public double CalcRadiusByMass(double m2)
        {
            return RadiusForOneMass * Math.Pow(m2, 1.0 / 3.0); // m1=1
        }
    }

    public enum InitVelocityKind
    {
        None,
        Random,
        Tangent
    }

    public class Galaxy
    {
        public int XCenter { get; set; } = 0;
        public int YCenter { get; set; } = 0;
        public int ZCenter { get; set; } = 0;
        public int XBox { get; set; } = 1000;
        public int YBox { get; set; } = 1000;
        public int ZBox { get; set; } = 500;
        public int PlanetCount { get; set; } = 2000;
        public InitVelocityKind InitVelocityKind { get; set; }
        public double InitVelocity { get; set; } = 0.5;
        public double InitMass { get; set; } = 1;
        public bool DarkMatter { get; set; } = false;
    }

    public class StarPlanet
    {
        public double InitMass { get; set; } = 100;
        public int X { get; set; } = 10000;
        public int Y { get; set; } = 10000;
        public int Z { get; set; } = 5000;
        public double VelocityX { get; set; } = 0;
        public double VelocityY { get; set; } = 0;
        public double VelocityZ { get; set; } = 0;
        public bool DarkMatter { get; set; } = false;
    }
}