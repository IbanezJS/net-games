﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;

namespace SolarSystem
{
    public partial class SolarControl : UserControl
    {
        private SolarSimulation simulation;
        private readonly Settings settings;
        private WriteableBitmap bitmapWriter;
        private DrawManager drawManager;
        private Stopwatch timer;
        private int widthPx;
        private int heightPx;
        private double resizeFactorX = 1;
        private double resizeFactorY = 1;

        public SolarControl()
        {
            var json = File.ReadAllText("Settings/Settings.json", Encoding.UTF8);
            settings = JsonConvert.DeserializeObject<Settings>(json);
            
            InitializeComponent();
            
            DataContext = this;
            
            SpeedSlider.Minimum = Math.Log(1);
            SpeedSlider.Maximum = Math.Log(1000);
            SpeedSlider.Value = Math.Log(settings.MoveTimeMs);
            SpeedSlider.IsDirectionReversed = true;

            GravitySlider.Minimum = 0;
            GravitySlider.Maximum = 100;
            GravitySlider.Value = settings.GravityFactor;

            SetSpeed();
            SetGravityFactor();
        }
        
        private void MainImage_OnLoaded(object sender, RoutedEventArgs e)
        {
            widthPx = (int)MainGrid.ColumnDefinitions[1].ActualWidth;
            heightPx = (int)MainGrid.ActualHeight;

            bitmapWriter = BitmapFactory.New(widthPx, heightPx);
            MainImage.Source = bitmapWriter;

            CompositionTarget.Rendering += CompositionTargetRendering;
        }

        private void CompositionTargetRendering(object sender, EventArgs e)
        {
            Draw();
        }

        private void Draw()
        {
            if (drawManager == null)
                return;
            var lagMs = drawManager.TimeTick(timer.Elapsed.TotalMilliseconds);
            TextBlockLag.Text = lagMs > 0 ? $"Lag = {lagMs / 1000:0.00}s" : "Lag = 0s";
            TextBlockPlanetsCount.Text = $"Count = {simulation.PlanetCount}";
        }

        public bool PinToBiggest
        {
            get => drawManager?.PinToBiggest ?? false;
            set
            {
                if (drawManager != null)
                    drawManager.PinToBiggest = value;
            }
        }

        private void SpeedSliderOnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!IsLoaded)
                return;
            SetSpeed();
        }

        private void GravitySliderOnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!IsLoaded)
                return;
            SetGravityFactor();
        }

        private void SetSpeed()
        {
            settings.MoveTimeMs = Math.Pow(Math.E, SpeedSlider.Value);
            TextBlockSpeed.Text = $"Speed = {(int)settings.MoveTimeMs} ms/move";
            drawManager?.SetSpeed();
        }

        private void SetGravityFactor()
        {
            settings.GravityFactor = GravitySlider.Value;
            TextBlockGravity.Text = $"GravityFactor = {settings.GravityFactor}";
        }

        private void ButtonStartOnClick(object sender, RoutedEventArgs e)
        {
            if (settings.ThreadCount <= 0 || settings.ThreadCount >= Environment.ProcessorCount)
                settings.ThreadCount = Math.Max(1, Environment.ProcessorCount - 1); //1 thread for UI

            simulation?.StopSimulation();

            simulation = new SolarSimulation(settings);

            drawManager = new DrawManager(
                simulation, 
                settings,
                bitmapWriter,
                widthPx,
                heightPx, 
                Colors.Black //todo background color to settings
                );

            PinToBiggest = false;

            SetSpeed();
            SetGravityFactor();

            simulation.StartSimulation();
            
            timer = Stopwatch.StartNew();
        }

        private void MainImage_OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var pos = Mouse.GetPosition(MainImage);
            drawManager?.AddEntityToWorld((int)(pos.X * resizeFactorX), (int)(pos.Y * resizeFactorY));
        }

        private void MainImage_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount >= 2)
                drawManager?.ResetWorld();
            else 
            {
                var pos = Mouse.GetPosition(MainImage);
                drawManager?.StartRotating((int)(pos.X * resizeFactorX), (int)(pos.Y * resizeFactorY));
            }
        }

        private void MainImage_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            drawManager?.StopRotating();
        }

        private void MainImage_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (drawManager == null)
                return;
            if (e.LeftButton == MouseButtonState.Released)
                return;
            var pos = Mouse.GetPosition(MainImage);
            drawManager.OnRotating((int)(pos.X * resizeFactorX), (int)(pos.Y * resizeFactorY));
        }

        private void MainImage_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            drawManager?.Zoom(-e.Delta);
        }

        private void MainImage_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!IsLoaded)
                return;
            
            double newWidthPx = MainImage.ActualWidth;
            double newHeightPx = MainImage.ActualHeight;
            if (newWidthPx > 0 && newHeightPx > 0)
            {
                resizeFactorX = widthPx / newWidthPx;
                resizeFactorY = heightPx / newHeightPx;
            }
            else
            {
                resizeFactorX = 1;
                resizeFactorY = 1;
            }
        }

        private void EditSettingsClick(object sender, RoutedEventArgs e)
        {
            var window = new SettingsWindow(settings);
            window.ShowDialog();
        }
    }
}
