﻿using System;
using System.Diagnostics;
using System.Windows.Media;

namespace SolarSystem
{
    public static class Helper
    {
        public static Color Blend(this Color color, Color background, double opacity)
        {
            //opacity: 0 = transparent. 1 = not transparent
            double t = 1 - opacity;
            return Color.FromRgb(
                BlendChannel(color.R, background.R, t),
                BlendChannel(color.G, background.G, t),
                BlendChannel(color.B, background.B, t));

        }

        private static byte BlendChannel(byte aC, byte bC, double t)
        {
            double a = aC / 255.0;
            double b = bC / 255.0;
            double blend = Math.Sqrt((1 - t) * a * a + t * b * b);
            Debug.Assert( blend >= 0);
            Debug.Assert( blend <= 255);
            return (byte) (blend * 255.0);
        }
    }
}
