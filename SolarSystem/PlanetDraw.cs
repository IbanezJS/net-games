﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SolarSystem
{
    public class PlanetDraw
    {
        private Settings settings;
        private double xDrawFrom;
        private double yDrawFrom;
        private double xDrawTo;
        private double yDrawTo;
        private double timeSnapshotMs;
        private double radiusFrom;
        private double radiusTo;
        private int currentRadiusPx;
        private double maxT; // T = transparency
        private Color background;
        private readonly double veryMaxRadius;

        public PlanetDraw(double veryMaxRadius)
        {
            this.veryMaxRadius = veryMaxRadius;
        }

        public int CurrentXpx { get; private set; }
        public int CurrentYpx { get; private set; }
        public Color Color { get; private set; }

        public void Reset(Settings settings,
            double x1s, double y1s, double x2s, double y2s,
            double radiusScreenFrom, double radiusScreenTo, 
            double origRadius, Color backgroundColor , double elapsedMs)
        {
            this.settings = settings;
            xDrawFrom = x1s;
            yDrawFrom = y1s;
            xDrawTo = x2s;
            yDrawTo = y2s;
            radiusFrom = radiusScreenFrom;
            radiusTo = radiusScreenTo;
            background = backgroundColor;
            timeSnapshotMs = elapsedMs;
            maxT = Math.Max(radiusScreenFrom, radiusScreenTo) / 30.0; //todo magic
            maxT = Math.Min(1, maxT);
            maxT = Math.Max(0.5, maxT);
            if (origRadius < 0.1 * veryMaxRadius)
                Color = Colors.YellowGreen;
            else if (origRadius < 0.4 * veryMaxRadius)
            {
                var opacity = origRadius / (0.4 * veryMaxRadius);
                Color = Colors.Yellow.Blend(Colors.YellowGreen, opacity);
            }
            else if (origRadius < 0.75 * veryMaxRadius)
            {
                var opacity = origRadius / (0.75 * veryMaxRadius);
                Color = Colors.White.Blend(Colors.Yellow, opacity);
            }
            else
                Color = Colors.White;
        }

        public void SetTime(double elapsedMs)
        {
            double deltaMs = Math.Min(elapsedMs - timeSnapshotMs, settings.MoveTimeMs);
            double timeFactor = deltaMs / settings.MoveTimeMs;
            Debug.Assert((timeFactor >= 0) && (timeFactor <= 1));
            double xDelta = timeFactor * (xDrawTo - xDrawFrom);
            double yDelta = timeFactor * (yDrawTo - yDrawFrom);
            double rDelta = timeFactor * (radiusTo - radiusFrom);
            CurrentXpx = (int)Math.Round(xDrawFrom + xDelta);
            CurrentYpx = (int)Math.Round(yDrawFrom + yDelta);
            currentRadiusPx = (int)Math.Round(radiusFrom + rDelta);
        }

        public void DrawPlanet(WriteableBitmap bitmap)
        {
            int r = Math.Max(1, currentRadiusPx);
            if (r <= 8)
            {
                var opacity = maxT * r / 8.0;
                var color = Color.Blend(background, opacity);
                bitmap.FillEllipseCentered(CurrentXpx, CurrentYpx, r, r, color);
                return;
            }
            //todo magic
            double opacity3 = maxT * 0.6;
            var color3 = Color.Blend(background, opacity3);
            bitmap.FillEllipseCentered(CurrentXpx, CurrentYpx, r, r, color3);

            double opacity2 = maxT * 0.8;
            var color2 = Color.Blend(background, opacity2);
            int r2 = Math.Max(1, (int)(currentRadiusPx * 0.8));
            bitmap.FillEllipseCentered(CurrentXpx, CurrentYpx, r2, r2, color2);

            double opacity1 = maxT;
            var color1 = Color.Blend(background, opacity1);
            int r1 = Math.Max(1, (int)(currentRadiusPx * 0.7));
            bitmap.FillEllipseCentered(CurrentXpx, CurrentYpx, r1, r1, color1);
        }

        public void DrawPlanetSimple(WriteableBitmap bitmap)
        {
            int r = Math.Max(1, currentRadiusPx);
            bitmap.FillEllipseCentered(CurrentXpx, CurrentYpx, r, r, Color);
        }
    }
}
