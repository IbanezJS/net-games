﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SolarSystem
{
    public class WaveDraw
    {
        private Settings settings;
        private int xDrawPx;
        private int yDrawPx;
        private double timeSnapshotMs;
        private double radiusFrom;
        private double radiusTo;
        private double transparency = 0.1;
        private Color background;

        public WaveDraw() {}

        public int CurrentRpx { get; private set; }
        public Color Color { get; private set; } = Colors.Beige;

        public void Reset(Settings settings,
            double xs, double ys, 
            double rFrom, double rTo,
            Color backgroundColor, double elapsedMs)
        {
            Debug.Assert(rTo > rFrom);
            this.settings = settings;
            xDrawPx = (int)Math.Round(xs);
            yDrawPx = (int)Math.Round(ys);
            radiusFrom = rFrom;
            radiusTo = rTo;
            background = backgroundColor;
            timeSnapshotMs = elapsedMs;
        }

        public void SetTime(double elapsedMs)
        {
            double deltaMs = Math.Min(elapsedMs - timeSnapshotMs, settings.MoveTimeMs);
            double timeFactor = deltaMs / settings.MoveTimeMs;
            Debug.Assert((timeFactor >= 0) && (timeFactor <= 1));
            double rDelta = timeFactor * (radiusTo - radiusFrom);
            CurrentRpx = (int)Math.Round(radiusFrom + rDelta);
            Debug.Assert((CurrentRpx >= radiusFrom - 1) && (CurrentRpx <= radiusTo + 1));
        }

        public void DrawWave(WriteableBitmap bitmap)
        {
            double opacity = transparency;
            var color = Color.Blend(background, opacity);
            //var color = Colors.Blue;
            bitmap.DrawEllipseCentered(xDrawPx, yDrawPx, CurrentRpx, CurrentRpx, color);
            //bitmap.FillEllipseCentered(xDrawPx, yDrawPx, CurrentRpx, CurrentRpx, color);
        }
    }
}
