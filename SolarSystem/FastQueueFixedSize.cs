﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace SolarSystem
{
    public static class NumericHelper
    {
        public static int UpperPowerOfTwo(int num)
        {
            --num;
            num |= num >> 1;
            num |= num >> 2;
            num |= num >> 4;
            num |= num >> 8;
            num |= num >> 16;
            ++num;
            return num;
        }
    }

    /// <summary>
    /// It is a little bit refactored and simplified original .net Queue.
    /// WARNING: it is fixed size queue and unsafe (without size check)
    /// </summary>
    [DebuggerDisplay("Count = {Count}, MaxSize = {MaxSize}")]
    [DebuggerTypeProxy(typeof(FastQueueFixedSize<>.DebugView))]
    public class FastQueueFixedSize<T> where T : struct //only struct for simply clear
    {
        private readonly T[] buffer;
        private int head; // points to existing element (first head slot). head => left
        private int tail; // points to where to enqueue element (empty slot after last tail slot). tail => right

        public int Count { get; private set; }

        public bool Full => Count >= MaxSize;

        public readonly int MaxSize;
        private readonly int maxSizeMinusOne;

        public FastQueueFixedSize(int maxSizeHint)
        {
            MaxSize = NumericHelper.UpperPowerOfTwo(maxSizeHint);
            buffer = new T[MaxSize];
            maxSizeMinusOne = MaxSize - 1;
        }

        public void Clear(bool eraseInner)
        {
            if (eraseInner)
                Array.Clear(buffer, 0, buffer.Length);
            Count = 0;
            head = 0;
            tail = 0;
        }

        /// <summary>
        /// WARNING: do not check size
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Enqueue(T item)
        {
            //Capacity increase (don't do)
            buffer[tail] = item;
            tail = (tail + 1) & maxSizeMinusOne;
            ++Count; //if size > maxSize then override
            Debug.Assert(Count <= MaxSize);
        }

        /// <summary>
        /// WARNING: do not check size
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ref T EnqueueByRef()
        {
            //Capacity increase (don't do)
            int enqueueAt = tail;
            tail = (tail + 1) & maxSizeMinusOne;
            ++Count; //if size > maxSize then override
            Debug.Assert(Count <= MaxSize);
            return ref buffer[enqueueAt];
        }

        /// <summary>
        /// WARNING: do not check size
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Dequeue()
        {
            //if (this.size == 0)
            //    throw new InvalidOperationException(SR.InvalidOperation_EmptyQueue);
            T obj = buffer[head];
            head = (head + 1) & maxSizeMinusOne;
            --Count; //if size < 0 then undefined behaviour
            Debug.Assert(Count >= 0);
            return obj;
        }

        /// <summary>
        /// WARNING: do not check size
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ref T DequeueByRef()
        {
            //if (this.size == 0)
            //    throw new InvalidOperationException(SR.InvalidOperation_EmptyQueue);
            int dequeueAt = head;
            head = (head + 1) & maxSizeMinusOne;
            --Count; //if size < 0 then undefined behaviour
            Debug.Assert(Count >= 0);
            return ref buffer[dequeueAt];
        }

        /// <summary>
        /// WARNING: do not check size
        /// </summary>
        public void DequeueBatchSilently(int batchSize)
        {
            head = (head + batchSize) & maxSizeMinusOne;
            Count -= batchSize; //if size < 0 then undefined behaviour
            Debug.Assert(Count >= 0);
        }

        /// <summary>
        /// WARNING: do not check size
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Peek()
        {
            //if (this.size == 0)
            //    throw new InvalidOperationException(SR.InvalidOperation_EmptyQueue);
            return buffer[head];
        }

        /// <summary>
        /// WARNING: do not check size
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ref T PeekByRef()
        {
            //if (this.size == 0)
            //    throw new InvalidOperationException(SR.InvalidOperation_EmptyQueue);
            return ref buffer[head];
        }

        /// <summary>
        /// WARNING: Do not check index out of range!!!
        /// Important: 0 = head (where to dequeue from), count-1 = tail (last queued item)
        /// </summary>
        public ref T this[int i]
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get
            {
                Debug.Assert((i >= 0) && (i < Count));
                return ref buffer[(head + i) & maxSizeMinusOne];
            }
        }


        /// <summary>
        /// Gets a value indicating whether the buffer is "split" (meaning the beginning of the view is at a later index in <see cref="buffer"/> than the end).
        /// </summary>
        private bool IsSplit => head > MaxSize - Count;

        public void CopyToArray(T[] array, int arrayIndex = 0)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array));
            if (array.Length < Count + arrayIndex)
                throw new ArgumentOutOfRangeException(nameof(array), "Not enough length");
            if (IsSplit)
            {
                // The existing buffer is split, so we have to copy it in parts
                int length = MaxSize - head;
                Debug.Assert(tail == Count - length);
                Array.Copy(buffer, head, array, arrayIndex, length);
                Array.Copy(buffer, 0, array, arrayIndex + length, Count - length);
            }
            else
            {
                // The existing buffer is whole
                Debug.Assert(tail == head + Count);
                Array.Copy(buffer, head, array, arrayIndex, Count);
            }
        }

        [DebuggerNonUserCode]
        private sealed class DebugView
        {
            private readonly FastQueueFixedSize<T> queue;

            public DebugView(FastQueueFixedSize<T> queue)
            {
                this.queue = queue;
            }

            [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
            public T[] Items
            {
                get
                {
                    var arr = new T[queue.Count];
                    queue.CopyToArray(arr);
                    return arr;
                }
            }
        }
    }
}
