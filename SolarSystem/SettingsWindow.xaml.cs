﻿using System.Windows;

namespace SolarSystem
{
    public partial class SettingsWindow : Window
    {
        public SettingsWindow(Settings settings)
        {
            InitializeComponent();
            PropertyGrid.SelectedObject = settings;
        }
    }
}
