﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace SolarSystem
{
    public class DrawManager 
    {
        private readonly Random rnd;
        private readonly SolarSimulation simulation;
        private readonly Settings settings;
        private readonly WriteableBitmap bitmap;
        private readonly int bitmapWidthPx;
        private readonly int bitmapHeightPx;
        private readonly Color backgroundColor;
        private double lastMoveVirtualTimeMs;
        private readonly Stopwatch notDrawTimer = new Stopwatch();

        private readonly SortedDictionary<double, PlanetDraw> currentVisionPlanets; //sorted by z-buffer
        private readonly List<PlanetSnap> visiblePlanets;

        private readonly List<WaveDraw> currentVisionWaves;
        private readonly List<WaveSnap> visibleWaves;

        private PlanetSnap biggestPlanet;
        private readonly List<PlanetDraw> planetDrawPool; 
        private readonly List<WaveDraw> waveDrawPool; 
        private MoveInfo lastMoveInfo;

        //spherical
        private double currentR;
        private double currentCameraDelta;
        private double theta;
        private double phi;
        //[] = x,y,z
        private readonly double[] centre = new double[3]; //projection plane centre (x,y,z)
        private readonly double[] nR = new double[3]; //norm vecor R (z-buffer)
        private readonly double[] nTheta = new double[3]; //norm vecor theta (Y-projection)
        private readonly double[] nPhi = new double[3]; //norm vecor phi (X-projection)

        private double[][] stars;
        private List<int> starsVisionX;
        private List<int> starsVisionY;
        private List<Color> starsVisionColors;
        private int axesLength;

        private double rotateStartX = -1;
        private double rotateStartY = -1;
        private readonly double veryMaxRadius;

        public bool PinToBiggest { get; set; }

        public DrawManager(
            SolarSimulation simulation, 
            Settings settings,
            WriteableBitmap bitmap, 
            int bitmapWidthPx,
            int bitmapHeightPx,
            Color backgroundColor)
        {
            rnd = new Random(Guid.NewGuid().GetHashCode());
            this.simulation = simulation;
            this.settings = settings;
            this.bitmap = bitmap;
            this.bitmapWidthPx = bitmapWidthPx;
            this.bitmapHeightPx = bitmapHeightPx;
            this.backgroundColor = backgroundColor;

            currentVisionPlanets = new SortedDictionary<double, PlanetDraw>();
            visiblePlanets = new List<PlanetSnap>(simulation.PlanetCount);

            currentVisionWaves = new List<WaveDraw>(simulation.PlanetCount);
            visibleWaves = new List<WaveSnap>(simulation.PlanetCount);

            planetDrawPool = new List<PlanetDraw>(simulation.PlanetCount);
            waveDrawPool = new List<WaveDraw>(simulation.PlanetCount);
            
            if (settings.GalaxyList.Count > 0)
            {
                double maxInitRadius = settings.GalaxyList.Max(g => settings.CalcRadiusByMass(g.InitMass));
                int planetCount = settings.GalaxyList.Sum(g => g.PlanetCount);
                var r3 = Math.Pow(maxInitRadius, 3);
                veryMaxRadius = Math.Pow(planetCount * r3, 1.0 / 3.0);
            } else if (settings.StarPlanets.Count > 0)
            {
                veryMaxRadius = settings.StarPlanets.Max(p => settings.CalcRadiusByMass(p.InitMass));
            }
            
            for (int i = 0; i < simulation.PlanetCount; i++)
            {
                planetDrawPool.Add(new PlanetDraw(veryMaxRadius));
                waveDrawPool.Add(new WaveDraw());
            }

            InitCamera();
            CalcCameraAbsoluteBySpheric();

            InitStars();
            CalcStarsVision();
        }

        private void InitCamera()
        {
            currentR = settings.CameraRadius;
            currentCameraDelta = settings.CameraDelta;
            axesLength = (int) ((currentR + currentCameraDelta) * 0.6);
            phi = settings.CameraPhi * Math.PI / 180.0;
            theta = settings.CameraTheta * Math.PI / 180.0;
        }

        private void InitStars()
        {
            double starRadius = 1000000000;
            stars = new double[settings.MaxStarsCount][];
            for (int i = 0; i < stars.Length; i++)
            {
                double r = starRadius;
                stars[i] = new double[3];
                double x = (rnd.NextDouble() - 0.5) * 2 * r;
                r = Math.Sqrt(r * r - x * x);
                double y = (rnd.NextDouble() - 0.5) * 2 * r;
                r = Math.Sqrt(r * r - y * y);
                double z = (rnd.NextDouble() < 0.5 ? 1 : -1) * r;
                stars[i][0] = x;
                stars[i][1] = y;
                stars[i][2] = z;
                Debug.Assert(Math.Abs(Math.Sqrt(x*x + y*y + z*z) - starRadius) < 0.01);
            }
            starsVisionX = new List<int>();
            starsVisionY = new List<int>();
            starsVisionColors = new List<Color>();
        }

        private void CalcStarsVision()
        {
            starsVisionX.Clear();
            starsVisionY.Clear();
            starsVisionColors.Clear();
            double maxR = Math.Min(bitmapWidthPx, bitmapHeightPx)*0.5;
            for (int i = 0; i < stars.Length; i++)
            {
                var star = stars[i];
                TransformToProjection(star[0], star[1], star[2], out var x1, out var y1, out var z1, out _);
                if (z1 >= 0)
                    continue;
                TransformToScreen(x1, y1, out var xs, out var ys);
                if (xs > 0 && xs < bitmapWidthPx && ys > 0 && ys < bitmapHeightPx)
                {
                    var dx = xs - bitmapWidthPx * 0.5;
                    var dy = ys - bitmapHeightPx * 0.5;
                    var r = Math.Sqrt(dx * dx + dy * dy);
                    var opacity = 0.05 + r / maxR;
                    opacity = Math.Min(1, opacity);
                    var color = Colors.White.Blend(backgroundColor, opacity);
                    starsVisionX.Add((int)xs);
                    starsVisionY.Add((int)ys);
                    starsVisionColors.Add(color);
                }
            }
        }

        private void CalcCameraAbsoluteBySpheric()
        {
            var cosT = Math.Cos(theta);
            var sinT = Math.Sin(theta);
            var cosP = Math.Cos(phi);
            var sinP = Math.Sin(phi);
            
            centre[0] = currentR * cosP * sinT; //x
            centre[1] = currentR * sinP * sinT; //y
            centre[2] = currentR * cosT; //z

            nR[0] = cosP * sinT; 
            nR[1] = sinP * sinT; 
            nR[2] = cosT;

            nTheta[0] = cosP * cosT;
            nTheta[1] = sinP * cosT;
            nTheta[2] = -sinT;

            nPhi[0] = -sinP;
            nPhi[1] = cosP;
            nPhi[2] = 0;
        }

        public void Zoom(double deltaR)
        {
            if (Math.Abs(deltaR) > 200)
                deltaR = Math.Sign(deltaR) * 200;
            var rAbs = Math.Abs(currentR) + 1;
            var delta = (deltaR / 1000.0) * rAbs;
            delta = Math.Sign(delta)*Math.Max(Math.Abs(delta), settings.CameraDelta*0.5);
            var newR = currentR + delta;
            newR = Math.Max(0, newR);
            MoveCamera(newR, theta, phi);
        }

        public void StartRotating(int x, int y)
        {
            rotateStartX = x;
            rotateStartY = y;
        }

        public void StopRotating()
        {
            rotateStartX = -1;
            rotateStartY = -1;
        }

        public void OnRotating(int x, int y)
        {
            if (rotateStartX < 0 || rotateStartY < 0)
                return;
            double dx = rotateStartX - x;
            double dy = rotateStartY - y;
            double dPhi = Math.PI*(dx / bitmapWidthPx);
            double dTheta = Math.PI * (dy / bitmapHeightPx);
            rotateStartX = x;
            rotateStartY = y;
            MoveCamera(currentR, theta + dTheta, phi + dPhi);
        }

        private void MoveCamera(double newR, double newTheta, double newPhi)
        {
            currentR = newR;
            theta = newTheta;
            phi = newPhi;
            CalcCameraAbsoluteBySpheric();
            CalcStarsVision();
            RecalcDrawPlanets(visiblePlanets, false);
            RecalcDrawWaves(visibleWaves, false);
        }

        private void RecalcDrawPlanets(IEnumerable<PlanetSnap> planets, bool addToVisible)
        {
            int index = 0;
            currentVisionPlanets.Clear();
            if (addToVisible)
                visiblePlanets.Clear();
            foreach (var planet in planets)
            {
                if (!FilterSnap(planet, out var x1s, out var y1s, out var x2s, out var y2s,
                                out var radiusScreenFrom, out var radiusScreenTo, out var maxZp))
                    continue;
                var planetDraw = planetDrawPool[index++]; // TODO out of range ?
                planetDraw.Reset(settings, x1s, y1s, x2s, y2s,
                    radiusScreenFrom, radiusScreenTo, planet.Radius,
                    backgroundColor, lastMoveVirtualTimeMs);
                currentVisionPlanets.Add(maxZp, planetDraw);
                if (addToVisible)
                    visiblePlanets.Add(planet);
            }
        }

        private void RecalcDrawWaves(IEnumerable<WaveSnap> waves, bool addToVisible)
        {
            if (!settings.DrawGravityWaves)
                return;
            int index = 0;
            currentVisionWaves.Clear();
            if (addToVisible)
                visibleWaves.Clear();
            foreach (var wave in waves)
            {
                if (!FilterWave(wave, out var xs, out var ys, out var radiusWaveFrom, out var radiusWaveTo))
                    continue;
                var waveDraw = waveDrawPool[index++]; // TODO out of range ?
                waveDraw.Reset(settings, xs, ys, radiusWaveFrom, radiusWaveTo, backgroundColor, lastMoveVirtualTimeMs);
                currentVisionWaves.Add(waveDraw);
                if (addToVisible)
                    visibleWaves.Add(wave);
            }
        }

        private void TransformToProjection(double x, double y, double z, out double xP, out double yP, out double zP, out double factor)
        {
            if (PinToBiggest)
            {
                //convert to relative coordinates
                x -= biggestPlanet.Xto;
                y -= biggestPlanet.Yto;
                z -= biggestPlanet.Zto;
            }

            xP = (x - centre[0]) * nPhi[0] + (y - centre[1]) * nPhi[1]; // + (z - centre[2]) * nPhi[2]; => nPhi[2] always 0
            yP = (x - centre[0]) * nTheta[0] + (y - centre[1]) * nTheta[1] + (z - centre[2]) * nTheta[2];
            zP = (x - centre[0]) * nR[0] + (y - centre[1]) * nR[1] + (z - centre[2]) * nR[2];

            var div = currentCameraDelta - zP;
            if (div <= 0)
                div = 1;
            factor = currentCameraDelta / div;
            xP *= factor;
            yP *= factor;
        }

        private void TransformToGlobal(double xP, double yP, double zP, out double x, out double y, out double z)
        {
            var backFactor = (currentCameraDelta - zP) / currentCameraDelta;
            xP *= backFactor; 
            yP *= backFactor;
            var cosT = Math.Cos(theta);
            var sinT = Math.Sin(theta);
            var cosP = Math.Cos(phi);
            var sinP = Math.Sin(phi);
            var nx_R = sinT * cosP;
            var nx_T = cosT * cosP;
            var nx_P = -sinP;
            var ny_R = sinT * sinP;
            var ny_T = cosT * sinP;
            var ny_P = cosP;
            var nz_R = cosT;
            var nz_T = -sinT;
            //var nz_P = 0;
            x = (xP - 0) * nx_P + (yP - 0) * nx_T + (zP + currentR) * nx_R;
            y = (xP - 0) * ny_P + (yP - 0) * ny_T + (zP + currentR) * ny_R;
            z = /*(xP - 0) * nz_P*/ + (yP - 0) * nz_T + (zP + currentR) * nz_R;  // nz_P always 0

            if (PinToBiggest)
            {
                //convert to relative coordinates
                x += biggestPlanet.Xto;
                y += biggestPlanet.Yto;
                z += biggestPlanet.Zto;
            }
        }

        private void TransformToScreen(double xP, double yP, out double xS, out double yS)
        {
            xS = bitmapWidthPx * 0.5 + xP;
            yS = bitmapHeightPx * 0.5 + yP;
        }

        private void TransformFromScreen(double xS, double yS, out double xP, out double yP)
        {
            xP = xS - bitmapWidthPx * 0.5;
            yP = yS - bitmapHeightPx * 0.5;
        }

        public double TimeTick(double elapsedMs)
        {
            double lagMs = elapsedMs - lastMoveVirtualTimeMs;
            if (settings.TrySkipTimeLag)
                notDrawTimer.Restart();

            if (elapsedMs - lastMoveVirtualTimeMs > settings.MoveTimeMs)
            {
                if (lastMoveInfo != null)
                    simulation.ReleaseMove(lastMoveInfo);
                lastMoveInfo = null;

                while (simulation.MovesCache.TryDequeue(out var data))
                {
                    if (lastMoveInfo != null)
                        simulation.ReleaseMove(lastMoveInfo);
                    lastMoveInfo = data;
                    lastMoveVirtualTimeMs += settings.MoveTimeMs; //important: aligned time
                    lagMs = elapsedMs - lastMoveVirtualTimeMs;
                    if (!settings.TrySkipTimeLag)
                        break;
                    if (lagMs < 2 * settings.MoveTimeMs)
                        break;
                    if (notDrawTimer.ElapsedMilliseconds > 200) //min 5 draw per second)
                        break;
                }

                if (lastMoveInfo == null)
                    return lagMs; //no pre calculated moves

                RecalcDrawPlanets(lastMoveInfo.Planets, true);
                RecalcDrawWaves(lastMoveInfo.Waves, true);
                biggestPlanet = lastMoveInfo.BiggestPlanet;
            }

            var elapsedVirtualMs = Math.Min(elapsedMs, lastMoveVirtualTimeMs + settings.MoveTimeMs);
            DrawWorld(elapsedVirtualMs);
            
            return lagMs;
        }

        private bool FilterSnap(PlanetSnap planet
            , out double x1s, out double y1s, out double x2s, out double y2s
            , out double radiusScreenFrom, out double radiusScreenTo, out double maxZp)
        {
            // s = screen; p = projection
            x1s = y1s = x2s = y2s = 0;
            radiusScreenFrom = radiusScreenTo = 0;
            maxZp = 0;
            TransformToProjection(
                planet.Xfrom, planet.Yfrom, planet.Zfrom,
                out var x1p, out var y1p, out var z1p, out var f1);
            if (z1p >= currentCameraDelta)
                return false;
            TransformToProjection(planet.Xto, planet.Yto, planet.Zto,
                out var x2p, out var y2p, out var z2p, out var f2);
            if (z2p >= currentCameraDelta)
                return false;

            //double f = (f1 + f2) / 2;
            //radius = planet.Radius * f;

            TransformToProjection(
                  planet.Xfrom + planet.Radius
                , planet.Yfrom + planet.Radius
                , planet.Zfrom
                , out var xx1p, out var yy1p, out var zz1p, out var dummy1);
            
            TransformToProjection(
                  planet.Xto + planet.Radius
                , planet.Yto + planet.Radius
                , planet.Zto
                , out var xx2p, out var yy2p, out var zz2p, out var dummy2);

            TransformToScreen(x1p, y1p, out x1s, out y1s);
            TransformToScreen(x2p, y2p, out x2s, out y2s);
            TransformToScreen(xx1p, yy1p, out var xx1s, out var yy1s);
            TransformToScreen(xx2p, yy2p, out var xx2s, out var yy2s);

            double radiusScreenX1 = Math.Abs(x1s - xx1s);
            double radiusScreenY1 = Math.Abs(y1s - yy1s);

            double radiusScreenX2 = Math.Abs(x2s - xx2s);
            double radiusScreenY2 = Math.Abs(y2s - yy2s);

            if (Math.Abs(radiusScreenX1 - radiusScreenY1) > 0.5)
            {
                var h = 0;
            }
            if (Math.Abs(radiusScreenX2 - radiusScreenY2) > 0.5)
            {
                var h = 0;
            }

            radiusScreenFrom = (radiusScreenX1 + radiusScreenY1) * 0.5;
            radiusScreenTo = (radiusScreenX2 + radiusScreenY2) * 0.5;

            double maxRadius = Math.Max(radiusScreenFrom, radiusScreenTo);
            
            if (OutOfBound(x1s, y1s, maxRadius))
                return false;
            
            if (OutOfBound(x2s, y2s, maxRadius))
                return false;

            maxZp = Math.Max(z1p, z2p);
            maxZp += rnd.Next(1, 1000000) * 1e-6; //ensure unique key
            return true;
        }

        private bool FilterWave(WaveSnap wave
            , out double xs, out double ys
            , out double radiusWaveFrom, out double radiusWaveTo)
        {
            // s = screen; p = projection
            xs = ys = 0;
            radiusWaveFrom = radiusWaveTo = 0;
            TransformToProjection(wave.X, wave.Y, wave.Z, out var xp, out var yp, out var zp, out var f);
            if (zp >= currentCameraDelta)
                return false;
            radiusWaveFrom = wave.RadiusFrom * f;
            radiusWaveTo = wave.RadiusTo * f;
            TransformToScreen(xp, yp, out xs, out ys);
            if (OutOfBound(xs, ys, radiusWaveTo))
                return false;
            return true;
        }

        private bool OutOfBound(double xs, double ys, double radiusScreen)
        {
            double leftX = xs - radiusScreen;
            double rightX = xs + radiusScreen;
            double upY = ys - radiusScreen;
            double downY = ys + radiusScreen;
            if (rightX <= 0)
                return true;
            if (leftX >= bitmapWidthPx)
                return true;
            if (downY <= 0)
                return true;
            if (upY >= bitmapHeightPx)
                return true;
            return false;
        }

        private void DrawWorld(double elapsedVirtualMs)
        {
            bitmap.Clear();
            bitmap.FillRectangle(0, 0, bitmapWidthPx, bitmapHeightPx, backgroundColor);

            DrawStars();
            
            if (settings.DrawAxes)
                DrawAxes();

            DrawWaves(elapsedVirtualMs);
            DrawPlanets(elapsedVirtualMs);

            bitmap.DrawRectangle(0, 0, bitmapWidthPx, bitmapHeightPx, Colors.Yellow);
        }

        private void DrawAxes()
        {
            int axesLen = (int)Math.Min(axesLength, currentR + currentCameraDelta);
            
            TransformToProjection(0, 0, 0, out var xC, out var yC, out var zC, out _);
            TransformToProjection(axesLen, 0, 0, out var xX, out var yX, out var zX, out _);
            TransformToProjection(0, axesLen, 0, out var xY, out var yY, out var zY, out _);
            TransformToProjection(0, 0, axesLen, out var xZ, out var yZ, out var zZ, out _);

            TransformToScreen(xC, yC, out var xCs, out var yCs);
            TransformToScreen(xX, yX, out var xXs, out var yXs);
            TransformToScreen(xY, yY, out var xYs, out var yYs);
            TransformToScreen(xZ, yZ, out var xZs, out var yZs);

            bitmap.DrawLine((int)xCs, (int)yCs, (int)xXs, (int)yXs, Colors.Red);   //OX
            bitmap.DrawLine((int)xCs, (int)yCs, (int)xYs, (int)yYs, Colors.White); //OY
            bitmap.DrawLine((int)xCs, (int)yCs, (int)xZs, (int)yZs, Colors.Blue);  //OZ
        }

        private void DrawStars()
        {
            for (int i = 0; i < starsVisionX.Count; i++)
            {

                var xs = starsVisionX[i];
                var ys = starsVisionY[i];
                var color = starsVisionColors[i];
                bitmap.FillEllipseCentered(xs, ys, 1, 1, color);
            }
        }

        private void DrawPlanets(double elapsedVirtualMs)
        {
            if (currentVisionPlanets.Count == 1)
            {
                var g = 0;
            }
            foreach (var kvp in currentVisionPlanets)
            {
                var planetDraw = kvp.Value;
                planetDraw.SetTime(elapsedVirtualMs);
                if (settings.UseSimpleDraw)
                    planetDraw.DrawPlanetSimple(bitmap);
                else
                    planetDraw.DrawPlanet(bitmap);
            }
        }

        private void DrawWaves(double elapsedVirtualMs)
        {
            if (!settings.DrawGravityWaves)
                return;
            foreach (var waveDraw in currentVisionWaves)
            {
                waveDraw.SetTime(elapsedVirtualMs);
                waveDraw.DrawWave(bitmap);
            }
        }

        public void AddEntityToWorld(int xPx, int yPx)
        {
            planetDrawPool.Add(new PlanetDraw(veryMaxRadius)); //ensure draw capacity
            TransformFromScreen(xPx, yPx, out var xP, out var yP);

            var dx = xPx - (bitmapWidthPx / 2.0);
            var dy = yPx - (bitmapHeightPx / 2.0);
            var max = Math.Min(bitmapWidthPx / 2.0, bitmapHeightPx / 2.0);
            var d = Math.Sqrt(dx * dx + dy * dy);
            d = Math.Min(d, max);
            double zP = ((d / max) - 1) * currentR;
            //zP = 0;
            TransformToGlobal(xP, yP, zP, out var x, out var y, out var z);
            simulation.AddPlanetDynamic(x, y, z); 
        }

        public void SetSpeed()
        {
            if (settings.MoveTimeMs >= 1 && settings.MoveTimeMs <= 100)
                simulation.SetPreCalcCapacity((int)(100.0/Math.Log(settings.MoveTimeMs + 1)));
            else
                simulation.SetPreCalcCapacity(0);
        }

        public void ResetWorld()
        {
            InitCamera();
            CalcCameraAbsoluteBySpheric();
            CalcStarsVision();
        }
    }
}